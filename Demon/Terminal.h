#pragma once

#include <Windows.h>
#include <iostream>
#include <sstream>
#include <locale>
#include <codecvt>

namespace Demon {
  enum TERMINAL_COLOR_UID {
    BLACK,
    BLUE,
    GREEN,
    CYAN,
    RED,
    PURPLE,
    YELLOW,
    GRAY,
    LGRAY,
    LBLUE,
    LGREEN,
    LCYAN,
    LRED,
    LPURPLE,
    LYELLOW,
    WHITE,
    TERMINAL_COLOR_UID_MAX
  };

  class Terminal {
    int foreground = GRAY;
    int background = BLACK;
    std::wstringstream* stringStream = nullptr;

  public:
    static Terminal& begin( std::wstringstream* stream = nullptr ) {
      getInstance().stringStream = stream;
      return getInstance();
    }

    Terminal& out( const char* value ) {
      if( stringStream )
        return out( ansiToUnicode( value ) );

      std::cout << value;
      return *this;
    }

    Terminal& out( const std::string& value ) {
      if( stringStream )
        return out( ansiToUnicode( value.c_str() ) );

      std::cout << value.c_str();
      return *this;
    }

    Terminal& out( const wchar_t* value ) {
      stringStream ? *stringStream << value :
        std::wcout << value;

      return *this;
    }

    Terminal& out( const std::wstring& value ) {
      stringStream ? *stringStream << value.c_str() :
        std::wcout << value;

      return *this;
    }

    template<class T>
    Terminal& out( const T& value ) {
      stringStream ? *stringStream << value :
        std::wcout << value;

      return *this;
    }

    Terminal& setForeground( int colorUID ) {
      foreground = colorUID != -1 ? colorUID : WHITE;
      updateConsoleColor();
      return *this;
    }

    Terminal& setBackground( int colorUID ) {
      background = colorUID != -1 ? colorUID : BLACK;
      updateConsoleColor();
      return *this;
    }

    Terminal& endline( int count = 1 ) {
      for( int i = 0; i < count; i++ )
        std::cout << std::endl;
      return *this;
    }

    Terminal& tabulate( int count = 1 ) {
      for( int i = 0; i < count; i++ )
        std::cout << "\t";
      return *this;
    }

    Terminal& resetColors() {
      foreground = GRAY;
      background = BLACK;
      updateConsoleColor();
      return *this;
    }
  private:
    static Terminal& getInstance() {
      static Terminal instance;
      return instance;
    }

    void updateConsoleColor() {
      HANDLE consoleHandle = GetStdHandle( STD_OUTPUT_HANDLE );
      SetConsoleTextAttribute( consoleHandle, foreground + (background << 4) );
    }

    std::wstring ansiToUnicode( const char* value ) {
      size_t size = mbstowcs( nullptr, value, 0 );
      if( size == -1 )
        return L"<err>";
      
      wchar_t* wideString = new wchar_t[size + 1];
      if( mbstowcs_s( nullptr, wideString, size + 1, value, size ) != 0 ) {
        delete[] wideString;
        return L"<err>";
      }

      std::wstring result = wideString;
      delete[] wideString;
      return result;
    }
  };
}