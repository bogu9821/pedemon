#pragma once

#include "Exception.h"
#include "Constants.h"

namespace Demon {
  class Processor {
    template<typename T>
    inline static int Comparison( const T& l, const T& r ) {
      return l == r ? 0 : l < r ? -1 : +1;
    }
  public:
    static void ResolveOperation( TYPE_UID type, INSTRUCTION_UID instruction, mem_t left, mem_t right, mem_t out ) {
      switch( instruction ) {
        case INSTRUCTION_MOV:
        {
          switch( type )
          {
            case TYPE_VOID_PTR: *reinterpret_cast<mem_t*>(out)    = *reinterpret_cast<mem_t*>(right);    break;
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = *reinterpret_cast<int8_t*>(right);   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = *reinterpret_cast<uint8_t*>(right);  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = *reinterpret_cast<int16_t*>(right);  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = *reinterpret_cast<uint16_t*>(right); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = *reinterpret_cast<int32_t*>(right);  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = *reinterpret_cast<uint32_t*>(right); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = *reinterpret_cast<int64_t*>(right);  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = *reinterpret_cast<uint64_t*>(right); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = *reinterpret_cast<long_t*>(right);   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = *reinterpret_cast<ulong_t*>(right);  break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = *reinterpret_cast<float*>(right);    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = *reinterpret_cast<double*>(right);   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( type ).Throw();
          }
          break;
        }
        case INSTRUCTION_ADD:
        {
          switch( type )
          {
            case TYPE_VOID_PTR: *reinterpret_cast<ulong_t*>(out)  = *reinterpret_cast<ulong_t*>(left)  + *reinterpret_cast<ulong_t*>(right);  break;
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = *reinterpret_cast<int8_t*>(left)   + *reinterpret_cast<int8_t*>(right);   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = *reinterpret_cast<uint8_t*>(left)  + *reinterpret_cast<uint8_t*>(right);  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = *reinterpret_cast<int16_t*>(left)  + *reinterpret_cast<int16_t*>(right);  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = *reinterpret_cast<uint16_t*>(left) + *reinterpret_cast<uint16_t*>(right); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = *reinterpret_cast<int32_t*>(left)  + *reinterpret_cast<int32_t*>(right);  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = *reinterpret_cast<uint32_t*>(left) + *reinterpret_cast<uint32_t*>(right); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = *reinterpret_cast<int64_t*>(left)  + *reinterpret_cast<int64_t*>(right);  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = *reinterpret_cast<uint64_t*>(left) + *reinterpret_cast<uint64_t*>(right); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = *reinterpret_cast<long_t*>(left)   + *reinterpret_cast<long_t*>(right);   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = *reinterpret_cast<ulong_t*>(left)  + *reinterpret_cast<ulong_t*>(right);  break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = *reinterpret_cast<float*>(left)    + *reinterpret_cast<float*>(right);    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = *reinterpret_cast<double*>(left)   + *reinterpret_cast<double*>(right);   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( type ).Throw();
          }
          break;
        }
        case INSTRUCTION_SUB:
        {
          switch( type )
          {
            case TYPE_VOID_PTR: *reinterpret_cast<ulong_t*>(out)  = *reinterpret_cast<ulong_t*>(left)  - *reinterpret_cast<ulong_t*>(right);  break;
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = *reinterpret_cast<int8_t*>(left)   - *reinterpret_cast<int8_t*>(right);   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = *reinterpret_cast<uint8_t*>(left)  - *reinterpret_cast<uint8_t*>(right);  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = *reinterpret_cast<int16_t*>(left)  - *reinterpret_cast<int16_t*>(right);  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = *reinterpret_cast<uint16_t*>(left) - *reinterpret_cast<uint16_t*>(right); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = *reinterpret_cast<int32_t*>(left)  - *reinterpret_cast<int32_t*>(right);  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = *reinterpret_cast<uint32_t*>(left) - *reinterpret_cast<uint32_t*>(right); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = *reinterpret_cast<int64_t*>(left)  - *reinterpret_cast<int64_t*>(right);  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = *reinterpret_cast<uint64_t*>(left) - *reinterpret_cast<uint64_t*>(right); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = *reinterpret_cast<long_t*>(left)   - *reinterpret_cast<long_t*>(right);   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = *reinterpret_cast<ulong_t*>(left)  - *reinterpret_cast<ulong_t*>(right);  break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = *reinterpret_cast<float*>(left)    - *reinterpret_cast<float*>(right);    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = *reinterpret_cast<double*>(left)   - *reinterpret_cast<double*>(right);   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( type ).Throw();
          }
          break;
        }
        case INSTRUCTION_MUL:
        {
          switch( type )
          {
            case TYPE_VOID_PTR: Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = *reinterpret_cast<int8_t*>(left)   * *reinterpret_cast<int8_t*>(right);   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = *reinterpret_cast<uint8_t*>(left)  * *reinterpret_cast<uint8_t*>(right);  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = *reinterpret_cast<int16_t*>(left)  * *reinterpret_cast<int16_t*>(right);  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = *reinterpret_cast<uint16_t*>(left) * *reinterpret_cast<uint16_t*>(right); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = *reinterpret_cast<int32_t*>(left)  * *reinterpret_cast<int32_t*>(right);  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = *reinterpret_cast<uint32_t*>(left) * *reinterpret_cast<uint32_t*>(right); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = *reinterpret_cast<int64_t*>(left)  * *reinterpret_cast<int64_t*>(right);  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = *reinterpret_cast<uint64_t*>(left) * *reinterpret_cast<uint64_t*>(right); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = *reinterpret_cast<long_t*>(left)   * *reinterpret_cast<long_t*>(right);   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = *reinterpret_cast<ulong_t*>(left)  * *reinterpret_cast<ulong_t*>(right);  break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = *reinterpret_cast<float*>(left)    * *reinterpret_cast<float*>(right);    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = *reinterpret_cast<double*>(left)   * *reinterpret_cast<double*>(right);   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( type ).Throw();
          }
          break;
        }
        case INSTRUCTION_DIV:
        {
          switch( type )
          {
            case TYPE_VOID_PTR: Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = *reinterpret_cast<int8_t*>(left)   / *reinterpret_cast<int8_t*>(right);   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = *reinterpret_cast<uint8_t*>(left)  / *reinterpret_cast<uint8_t*>(right);  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = *reinterpret_cast<int16_t*>(left)  / *reinterpret_cast<int16_t*>(right);  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = *reinterpret_cast<uint16_t*>(left) / *reinterpret_cast<uint16_t*>(right); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = *reinterpret_cast<int32_t*>(left)  / *reinterpret_cast<int32_t*>(right);  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = *reinterpret_cast<uint32_t*>(left) / *reinterpret_cast<uint32_t*>(right); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = *reinterpret_cast<int64_t*>(left)  / *reinterpret_cast<int64_t*>(right);  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = *reinterpret_cast<uint64_t*>(left) / *reinterpret_cast<uint64_t*>(right); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = *reinterpret_cast<long_t*>(left)   / *reinterpret_cast<long_t*>(right);   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = *reinterpret_cast<ulong_t*>(left)  / *reinterpret_cast<ulong_t*>(right);  break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = *reinterpret_cast<float*>(left)    / *reinterpret_cast<float*>(right);    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = *reinterpret_cast<double*>(left)   / *reinterpret_cast<double*>(right);   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( type ).Throw();
          }
          break;
        }
        case INSTRUCTION_MOD:
        {
          switch( type )
          {
            case TYPE_VOID_PTR: Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = *reinterpret_cast<int8_t*>(left)   % *reinterpret_cast<int8_t*>(right);   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = *reinterpret_cast<uint8_t*>(left)  % *reinterpret_cast<uint8_t*>(right);  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = *reinterpret_cast<int16_t*>(left)  % *reinterpret_cast<int16_t*>(right);  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = *reinterpret_cast<uint16_t*>(left) % *reinterpret_cast<uint16_t*>(right); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = *reinterpret_cast<int32_t*>(left)  % *reinterpret_cast<int32_t*>(right);  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = *reinterpret_cast<uint32_t*>(left) % *reinterpret_cast<uint32_t*>(right); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = *reinterpret_cast<int64_t*>(left)  % *reinterpret_cast<int64_t*>(right);  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = *reinterpret_cast<uint64_t*>(left) % *reinterpret_cast<uint64_t*>(right); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = *reinterpret_cast<long_t*>(left)   % *reinterpret_cast<long_t*>(right);   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = *reinterpret_cast<ulong_t*>(left)  % *reinterpret_cast<ulong_t*>(right);  break;
            case TYPE_FLOAT:    Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_DOUBLE:   Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            default: Exception().Append( "Unknown operand type " ).AppendHex( type ).Throw();
          }
          break;
        }
        case INSTRUCTION_LSHIFT:
        {
          switch( type )
          {
            case TYPE_VOID_PTR: Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = *reinterpret_cast<int8_t*>(left)   << *reinterpret_cast<int8_t*>(right);   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = *reinterpret_cast<uint8_t*>(left)  << *reinterpret_cast<uint8_t*>(right);  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = *reinterpret_cast<int16_t*>(left)  << *reinterpret_cast<int16_t*>(right);  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = *reinterpret_cast<uint16_t*>(left) << *reinterpret_cast<uint16_t*>(right); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = *reinterpret_cast<int32_t*>(left)  << *reinterpret_cast<int32_t*>(right);  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = *reinterpret_cast<uint32_t*>(left) << *reinterpret_cast<uint32_t*>(right); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = *reinterpret_cast<int64_t*>(left)  << *reinterpret_cast<int64_t*>(right);  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = *reinterpret_cast<uint64_t*>(left) << *reinterpret_cast<uint64_t*>(right); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = *reinterpret_cast<long_t*>(left)   << *reinterpret_cast<long_t*>(right);   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = *reinterpret_cast<ulong_t*>(left)  << *reinterpret_cast<ulong_t*>(right);  break;
            case TYPE_FLOAT:    Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_DOUBLE:   Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            default: Exception().Append( "Unknown operand type " ).AppendHex( type ).Throw();
          }
          break;
        }
        case INSTRUCTION_RSHIFT:
        {
          switch( type )
          {
            case TYPE_VOID_PTR: Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = *reinterpret_cast<int8_t*>(left)   >> *reinterpret_cast<int8_t*>(right);   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = *reinterpret_cast<uint8_t*>(left)  >> *reinterpret_cast<uint8_t*>(right);  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = *reinterpret_cast<int16_t*>(left)  >> *reinterpret_cast<int16_t*>(right);  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = *reinterpret_cast<uint16_t*>(left) >> *reinterpret_cast<uint16_t*>(right); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = *reinterpret_cast<int32_t*>(left)  >> *reinterpret_cast<int32_t*>(right);  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = *reinterpret_cast<uint32_t*>(left) >> *reinterpret_cast<uint32_t*>(right); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = *reinterpret_cast<int64_t*>(left)  >> *reinterpret_cast<int64_t*>(right);  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = *reinterpret_cast<uint64_t*>(left) >> *reinterpret_cast<uint64_t*>(right); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = *reinterpret_cast<long_t*>(left)   >> *reinterpret_cast<long_t*>(right);   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = *reinterpret_cast<ulong_t*>(left)  >> *reinterpret_cast<ulong_t*>(right);  break;
            case TYPE_FLOAT:    Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_DOUBLE:   Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            default: Exception().Append( "Unknown operand type " ).AppendHex( type ).Throw();
          }
          break;
        }
        case INSTRUCTION_BIT_AND:
        {
          switch( type )
          {
            case TYPE_VOID_PTR: Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = *reinterpret_cast<int8_t*>(left)   & *reinterpret_cast<int8_t*>(right);   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = *reinterpret_cast<uint8_t*>(left)  & *reinterpret_cast<uint8_t*>(right);  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = *reinterpret_cast<int16_t*>(left)  & *reinterpret_cast<int16_t*>(right);  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = *reinterpret_cast<uint16_t*>(left) & *reinterpret_cast<uint16_t*>(right); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = *reinterpret_cast<int32_t*>(left)  & *reinterpret_cast<int32_t*>(right);  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = *reinterpret_cast<uint32_t*>(left) & *reinterpret_cast<uint32_t*>(right); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = *reinterpret_cast<int64_t*>(left)  & *reinterpret_cast<int64_t*>(right);  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = *reinterpret_cast<uint64_t*>(left) & *reinterpret_cast<uint64_t*>(right); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = *reinterpret_cast<long_t*>(left)   & *reinterpret_cast<long_t*>(right);   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = *reinterpret_cast<ulong_t*>(left)  & *reinterpret_cast<ulong_t*>(right);  break;
            case TYPE_FLOAT:    Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_DOUBLE:   Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            default: Exception().Append( "Unknown operand type " ).AppendHex( type ).Throw();
          }
          break;
        }
        case INSTRUCTION_BIT_XOR:
        {
          switch( type )
          {
            case TYPE_VOID_PTR: Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = *reinterpret_cast<int8_t*>(left)   ^ *reinterpret_cast<int8_t*>(right);   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = *reinterpret_cast<uint8_t*>(left)  ^ *reinterpret_cast<uint8_t*>(right);  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = *reinterpret_cast<int16_t*>(left)  ^ *reinterpret_cast<int16_t*>(right);  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = *reinterpret_cast<uint16_t*>(left) ^ *reinterpret_cast<uint16_t*>(right); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = *reinterpret_cast<int32_t*>(left)  ^ *reinterpret_cast<int32_t*>(right);  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = *reinterpret_cast<uint32_t*>(left) ^ *reinterpret_cast<uint32_t*>(right); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = *reinterpret_cast<int64_t*>(left)  ^ *reinterpret_cast<int64_t*>(right);  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = *reinterpret_cast<uint64_t*>(left) ^ *reinterpret_cast<uint64_t*>(right); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = *reinterpret_cast<long_t*>(left)   ^ *reinterpret_cast<long_t*>(right);   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = *reinterpret_cast<ulong_t*>(left)  ^ *reinterpret_cast<ulong_t*>(right);  break;
            case TYPE_FLOAT:    Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_DOUBLE:   Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            default: Exception().Append( "Unknown operand type " ).AppendHex( type ).Throw();
          }
          break;
        }
        case INSTRUCTION_BIT_OR:
        {
          switch( type )
          {
            case TYPE_VOID_PTR: Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = *reinterpret_cast<int8_t*>(left)   | *reinterpret_cast<int8_t*>(right);   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = *reinterpret_cast<uint8_t*>(left)  | *reinterpret_cast<uint8_t*>(right);  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = *reinterpret_cast<int16_t*>(left)  | *reinterpret_cast<int16_t*>(right);  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = *reinterpret_cast<uint16_t*>(left) | *reinterpret_cast<uint16_t*>(right); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = *reinterpret_cast<int32_t*>(left)  | *reinterpret_cast<int32_t*>(right);  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = *reinterpret_cast<uint32_t*>(left) | *reinterpret_cast<uint32_t*>(right); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = *reinterpret_cast<int64_t*>(left)  | *reinterpret_cast<int64_t*>(right);  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = *reinterpret_cast<uint64_t*>(left) | *reinterpret_cast<uint64_t*>(right); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = *reinterpret_cast<long_t*>(left)   | *reinterpret_cast<long_t*>(right);   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = *reinterpret_cast<ulong_t*>(left)  | *reinterpret_cast<ulong_t*>(right);  break;
            case TYPE_FLOAT:    Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_DOUBLE:   Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            default: Exception().Append( "Unknown operand type " ).AppendHex( type ).Throw();
          }
          break;
        }
        case INSTRUCTION_COMPL:
        {
          switch( type )
          {
            case TYPE_VOID_PTR: Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = ~ *reinterpret_cast<int8_t*>(left);   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = ~ *reinterpret_cast<uint8_t*>(left);  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = ~ *reinterpret_cast<int16_t*>(left);  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = ~ *reinterpret_cast<uint16_t*>(left); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = ~ *reinterpret_cast<int32_t*>(left);  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = ~ *reinterpret_cast<uint32_t*>(left); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = ~ *reinterpret_cast<int64_t*>(left);  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = ~ *reinterpret_cast<uint64_t*>(left); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = ~ *reinterpret_cast<long_t*>(left);   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = ~ *reinterpret_cast<ulong_t*>(left);  break;
            case TYPE_FLOAT:    Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_DOUBLE:   Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            default: Exception().Append( "Unknown operand type " ).AppendHex( type ).Throw();
          }
          break;
        }
        case INSTRUCTION_NOT:
        {
          switch( type )
          {
            case TYPE_VOID_PTR: Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = ! *reinterpret_cast<int8_t*>(left);   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = ! *reinterpret_cast<uint8_t*>(left);  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = ! *reinterpret_cast<int16_t*>(left);  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = ! *reinterpret_cast<uint16_t*>(left); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = ! *reinterpret_cast<int32_t*>(left);  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = ! *reinterpret_cast<uint32_t*>(left); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = ! *reinterpret_cast<int64_t*>(left);  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = ! *reinterpret_cast<uint64_t*>(left); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = ! *reinterpret_cast<long_t*>(left);   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = ! *reinterpret_cast<ulong_t*>(left);  break;
            case TYPE_FLOAT:    Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_DOUBLE:   Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            default: Exception().Append( "Unknown operand type " ).AppendHex( type ).Throw();
          }
          break;
        }
        case INSTRUCTION_NEG:
        {
          switch( type )
          {
            case TYPE_VOID_PTR: Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = - *reinterpret_cast<int8_t*>(left);   break;
            case TYPE_UINT8:    Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = - *reinterpret_cast<int16_t*>(left);  break;
            case TYPE_UINT16:   Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = - *reinterpret_cast<int32_t*>(left);  break;
            case TYPE_UINT32:   Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = - *reinterpret_cast<int64_t*>(left);  break;
            case TYPE_UINT64:   Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = - *reinterpret_cast<long_t*>(left);   break;
            case TYPE_ULONG:    Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = - *reinterpret_cast<float*>(left);    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = - *reinterpret_cast<double*>(left);   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( type ).Throw();
          }
          break;
        }
        case INSTRUCTION_INC:
        {
          switch( type )
          {
            case TYPE_VOID_PTR: Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = *reinterpret_cast<int8_t*>(left)   + 1; break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = *reinterpret_cast<uint8_t*>(left)  + 1; break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = *reinterpret_cast<int16_t*>(left)  + 1; break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = *reinterpret_cast<uint16_t*>(left) + 1; break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = *reinterpret_cast<int32_t*>(left)  + 1; break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = *reinterpret_cast<uint32_t*>(left) + 1; break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = *reinterpret_cast<int64_t*>(left)  + 1; break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = *reinterpret_cast<uint64_t*>(left) + 1; break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = *reinterpret_cast<long_t*>(left)   + 1; break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = *reinterpret_cast<ulong_t*>(left)  + 1; break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = *reinterpret_cast<float*>(left)    + 1; break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = *reinterpret_cast<double*>(left)   + 1; break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( type ).Throw();
          }
          break;
        }
        case INSTRUCTION_DEC:
        {
          switch( type )
          {
            case TYPE_VOID_PTR: Exception().Append( "Illegal type in " ).Append( INSTRUCTION_NAME[instruction] ).Append( ": " ).AppendHex( type ).Throw();
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = *reinterpret_cast<int8_t*>(left)   - 1; break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = *reinterpret_cast<uint8_t*>(left)  - 1; break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = *reinterpret_cast<int16_t*>(left)  - 1; break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = *reinterpret_cast<uint16_t*>(left) - 1; break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = *reinterpret_cast<int32_t*>(left)  - 1; break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = *reinterpret_cast<uint32_t*>(left) - 1; break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = *reinterpret_cast<int64_t*>(left)  - 1; break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = *reinterpret_cast<uint64_t*>(left) - 1; break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = *reinterpret_cast<long_t*>(left)   - 1; break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = *reinterpret_cast<ulong_t*>(left)  - 1; break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = *reinterpret_cast<float*>(left)    - 1; break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = *reinterpret_cast<double*>(left)   - 1; break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( type ).Throw();
          }
        }
        case INSTRUCTION_CMP:
        {
          switch( type )
          {
            case TYPE_VOID_PTR: *reinterpret_cast<ulong_t*>(out)  = Comparison( *reinterpret_cast<ulong_t*>(left),  *reinterpret_cast<ulong_t*>(right) );  break;
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = Comparison( *reinterpret_cast<int8_t*>(left),   *reinterpret_cast<int8_t*>(right) );   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = Comparison( *reinterpret_cast<uint8_t*>(left),  *reinterpret_cast<uint8_t*>(right) );  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = Comparison( *reinterpret_cast<int16_t*>(left),  *reinterpret_cast<int16_t*>(right) );  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = Comparison( *reinterpret_cast<uint16_t*>(left), *reinterpret_cast<uint16_t*>(right) ); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = Comparison( *reinterpret_cast<int32_t*>(left),  *reinterpret_cast<int32_t*>(right) );  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = Comparison( *reinterpret_cast<uint32_t*>(left), *reinterpret_cast<uint32_t*>(right) ); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = Comparison( *reinterpret_cast<int64_t*>(left),  *reinterpret_cast<int64_t*>(right) );  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = Comparison( *reinterpret_cast<uint64_t*>(left), *reinterpret_cast<uint64_t*>(right) ); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = Comparison( *reinterpret_cast<long_t*>(left),   *reinterpret_cast<long_t*>(right) );   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = Comparison( *reinterpret_cast<ulong_t*>(left),  *reinterpret_cast<ulong_t*>(right) );  break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = Comparison( *reinterpret_cast<float*>(left),    *reinterpret_cast<float*>(right) );    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = Comparison( *reinterpret_cast<double*>(left),   *reinterpret_cast<double*>(right) );   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( type ).Throw();
          }
          break;
        }
        case INSTRUCTION_TEST:
        {
          switch( type )
          {
            case TYPE_VOID_PTR: *reinterpret_cast<ulong_t*>(out)  = *reinterpret_cast<ulong_t*>(left)  == *reinterpret_cast<ulong_t*>(right) ? 1 : 0; break;
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = *reinterpret_cast<int8_t*>(left)   == *reinterpret_cast<int8_t*>(right)   ? 1 : 0; break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = *reinterpret_cast<uint8_t*>(left)  == *reinterpret_cast<uint8_t*>(right)  ? 1 : 0; break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = *reinterpret_cast<int16_t*>(left)  == *reinterpret_cast<int16_t*>(right)  ? 1 : 0; break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = *reinterpret_cast<uint16_t*>(left) == *reinterpret_cast<uint16_t*>(right) ? 1 : 0; break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = *reinterpret_cast<int32_t*>(left)  == *reinterpret_cast<int32_t*>(right)  ? 1 : 0; break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = *reinterpret_cast<uint32_t*>(left) == *reinterpret_cast<uint32_t*>(right) ? 1 : 0; break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = *reinterpret_cast<int64_t*>(left)  == *reinterpret_cast<int64_t*>(right)  ? 1 : 0; break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = *reinterpret_cast<uint64_t*>(left) == *reinterpret_cast<uint64_t*>(right) ? 1 : 0; break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = *reinterpret_cast<long_t*>(left)   == *reinterpret_cast<long_t*>(right)   ? 1 : 0; break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = *reinterpret_cast<ulong_t*>(left)  == *reinterpret_cast<ulong_t*>(right)  ? 1 : 0; break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = *reinterpret_cast<float*>(left)    == *reinterpret_cast<float*>(right)    ? 1 : 0; break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = *reinterpret_cast<double*>(left)   == *reinterpret_cast<double*>(right)   ? 1 : 0; break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( type ).Throw();
          }
          break;
        }
      }
    }

    static void ResolveConversion( TYPE_UID type1, TYPE_UID type2, mem_t source, mem_t out ) {
      switch( type1 )
      {
        case TYPE_VOID_PTR:
        {
          switch( type2 )
          {
            case TYPE_VOID_PTR: *reinterpret_cast<mem_t*>(out)    = (mem_t)(*reinterpret_cast<mem_t*>(source));    break;
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = (int8_t)(*reinterpret_cast<mem_t*>(source));   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = (uint8_t)(*reinterpret_cast<mem_t*>(source));  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = (int16_t)(*reinterpret_cast<mem_t*>(source));  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = (uint16_t)(*reinterpret_cast<mem_t*>(source)); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = (int32_t)(*reinterpret_cast<mem_t*>(source));  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = (uint32_t)(*reinterpret_cast<mem_t*>(source)); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = (int64_t)(*reinterpret_cast<mem_t*>(source));  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = (uint64_t)(*reinterpret_cast<mem_t*>(source)); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = (long_t)(*reinterpret_cast<mem_t*>(source));   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = (ulong_t)(*reinterpret_cast<mem_t*>(source));  break;
            case TYPE_FLOAT:    Exception().Append( "Illegal conversion " ).Append( TYPE_NAME[type1] ).Append( " -> " ).Append( TYPE_NAME[type2] ).Throw();
            case TYPE_DOUBLE:   Exception().Append( "Illegal conversion " ).Append( TYPE_NAME[type1] ).Append( " -> " ).Append( TYPE_NAME[type2] ).Throw();
            default: Exception().Append( "Unknown operand type " ).AppendHex( (int)type2 ).Throw();
          }
          break;
        }
        case TYPE_INT8:
        {
          switch( type2 )
          {
            case TYPE_VOID_PTR: *reinterpret_cast<mem_t*>(out)    = (mem_t)(*reinterpret_cast<int8_t*>(source));               break;
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = static_cast<int8_t>(*reinterpret_cast<int8_t*>(source));   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = static_cast<uint8_t>(*reinterpret_cast<int8_t*>(source));  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = static_cast<int16_t>(*reinterpret_cast<int8_t*>(source));  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = static_cast<uint16_t>(*reinterpret_cast<int8_t*>(source)); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = static_cast<int32_t>(*reinterpret_cast<int8_t*>(source));  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = static_cast<uint32_t>(*reinterpret_cast<int8_t*>(source)); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = static_cast<int64_t>(*reinterpret_cast<int8_t*>(source));  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = static_cast<uint64_t>(*reinterpret_cast<int8_t*>(source)); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = static_cast<long_t>(*reinterpret_cast<int8_t*>(source));   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = static_cast<ulong_t>(*reinterpret_cast<int8_t*>(source));  break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = static_cast<float>(*reinterpret_cast<int8_t*>(source));    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = static_cast<double>(*reinterpret_cast<int8_t*>(source));   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( (int)type2 ).Throw();
          }
          break;
        }
        case TYPE_UINT8:
        {
          switch( type2 )
          {
            case TYPE_VOID_PTR: *reinterpret_cast<mem_t*>(out)    = (mem_t)(*reinterpret_cast<uint8_t*>(source));               break;
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = static_cast<int8_t>(*reinterpret_cast<uint8_t*>(source));   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = static_cast<uint8_t>(*reinterpret_cast<uint8_t*>(source));  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = static_cast<int16_t>(*reinterpret_cast<uint8_t*>(source));  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = static_cast<uint16_t>(*reinterpret_cast<uint8_t*>(source)); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = static_cast<int32_t>(*reinterpret_cast<uint8_t*>(source));  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = static_cast<uint32_t>(*reinterpret_cast<uint8_t*>(source)); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = static_cast<int64_t>(*reinterpret_cast<uint8_t*>(source));  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = static_cast<uint64_t>(*reinterpret_cast<uint8_t*>(source)); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = static_cast<long_t>(*reinterpret_cast<uint8_t*>(source));   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = static_cast<ulong_t>(*reinterpret_cast<uint8_t*>(source));  break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = static_cast<float>(*reinterpret_cast<uint8_t*>(source));    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = static_cast<double>(*reinterpret_cast<uint8_t*>(source));   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( (int)type2 ).Throw();
          }
          break;
        }
        case TYPE_INT16:
        {
          switch( type2 )
          {
            case TYPE_VOID_PTR: *reinterpret_cast<mem_t*>(out)    = (mem_t)(*reinterpret_cast<int16_t*>(source));               break;
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = static_cast<int8_t>(*reinterpret_cast<int16_t*>(source));   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = static_cast<uint8_t>(*reinterpret_cast<int16_t*>(source));  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = static_cast<int16_t>(*reinterpret_cast<int16_t*>(source));  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = static_cast<uint16_t>(*reinterpret_cast<int16_t*>(source)); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = static_cast<int32_t>(*reinterpret_cast<int16_t*>(source));  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = static_cast<uint32_t>(*reinterpret_cast<int16_t*>(source)); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = static_cast<int64_t>(*reinterpret_cast<int16_t*>(source));  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = static_cast<uint64_t>(*reinterpret_cast<int16_t*>(source)); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = static_cast<long_t>(*reinterpret_cast<int16_t*>(source));   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = static_cast<ulong_t>(*reinterpret_cast<int16_t*>(source));  break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = static_cast<float>(*reinterpret_cast<int16_t*>(source));    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = static_cast<double>(*reinterpret_cast<int16_t*>(source));   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( (int)type2 ).Throw();
          }
          break;
        }
        case TYPE_UINT16:
        {
          switch( type2 )
          {
            case TYPE_VOID_PTR: *reinterpret_cast<mem_t*>(out)    = (mem_t)(*reinterpret_cast<uint16_t*>(source));               break;
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = static_cast<int8_t>(*reinterpret_cast<uint16_t*>(source));   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = static_cast<uint8_t>(*reinterpret_cast<uint16_t*>(source));  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = static_cast<int16_t>(*reinterpret_cast<uint16_t*>(source));  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = static_cast<uint16_t>(*reinterpret_cast<uint16_t*>(source)); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = static_cast<int32_t>(*reinterpret_cast<uint16_t*>(source));  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = static_cast<uint32_t>(*reinterpret_cast<uint16_t*>(source)); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = static_cast<int64_t>(*reinterpret_cast<uint16_t*>(source));  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = static_cast<uint64_t>(*reinterpret_cast<uint16_t*>(source)); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = static_cast<long_t>(*reinterpret_cast<uint16_t*>(source));   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = static_cast<ulong_t>(*reinterpret_cast<uint16_t*>(source));  break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = static_cast<float>(*reinterpret_cast<uint16_t*>(source));    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = static_cast<double>(*reinterpret_cast<uint16_t*>(source));   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( (int)type2 ).Throw();
          }
          break;
        }
        case TYPE_INT32:
        {
          switch( type2 )
          {
            case TYPE_VOID_PTR: *reinterpret_cast<mem_t*>(out)    = (mem_t)(*reinterpret_cast<int32_t*>(source));               break;
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = static_cast<int8_t>(*reinterpret_cast<int32_t*>(source));   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = static_cast<uint8_t>(*reinterpret_cast<int32_t*>(source));  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = static_cast<int16_t>(*reinterpret_cast<int32_t*>(source));  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = static_cast<uint16_t>(*reinterpret_cast<int32_t*>(source)); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = static_cast<int32_t>(*reinterpret_cast<int32_t*>(source));  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = static_cast<uint32_t>(*reinterpret_cast<int32_t*>(source)); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = static_cast<int64_t>(*reinterpret_cast<int32_t*>(source));  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = static_cast<uint64_t>(*reinterpret_cast<int32_t*>(source)); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = static_cast<long_t>(*reinterpret_cast<int32_t*>(source));   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = static_cast<ulong_t>(*reinterpret_cast<int32_t*>(source));  break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = static_cast<float>(*reinterpret_cast<int32_t*>(source));    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = static_cast<double>(*reinterpret_cast<int32_t*>(source));   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( (int)type2 ).Throw();
          }
          break;
        }
        case TYPE_UINT32:
        {
          switch( type2 )
          {
            case TYPE_VOID_PTR: *reinterpret_cast<mem_t*>(out)    = (mem_t)(*reinterpret_cast<uint32_t*>(source));               break;
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = static_cast<int8_t>(*reinterpret_cast<uint32_t*>(source));   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = static_cast<uint8_t>(*reinterpret_cast<uint32_t*>(source));  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = static_cast<int16_t>(*reinterpret_cast<uint32_t*>(source));  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = static_cast<uint16_t>(*reinterpret_cast<uint32_t*>(source)); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = static_cast<int32_t>(*reinterpret_cast<uint32_t*>(source));  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = static_cast<uint32_t>(*reinterpret_cast<uint32_t*>(source)); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = static_cast<int64_t>(*reinterpret_cast<uint32_t*>(source));  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = static_cast<uint64_t>(*reinterpret_cast<uint32_t*>(source)); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = static_cast<long_t>(*reinterpret_cast<uint32_t*>(source));   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = static_cast<ulong_t>(*reinterpret_cast<uint32_t*>(source));  break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = static_cast<float>(*reinterpret_cast<uint32_t*>(source));    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = static_cast<double>(*reinterpret_cast<uint32_t*>(source));   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( (int)type2 ).Throw();
          }
          break;
        }
        case TYPE_INT64:
        {
          switch( type2 )
          {
            case TYPE_VOID_PTR: *reinterpret_cast<mem_t*>(out)    = (mem_t)(*reinterpret_cast<int64_t*>(source));               break;
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = static_cast<int8_t>(*reinterpret_cast<int64_t*>(source));   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = static_cast<uint8_t>(*reinterpret_cast<int64_t*>(source));  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = static_cast<int16_t>(*reinterpret_cast<int64_t*>(source));  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = static_cast<uint16_t>(*reinterpret_cast<int64_t*>(source)); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = static_cast<int32_t>(*reinterpret_cast<int64_t*>(source));  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = static_cast<uint32_t>(*reinterpret_cast<int64_t*>(source)); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = static_cast<int64_t>(*reinterpret_cast<int64_t*>(source));  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = static_cast<uint64_t>(*reinterpret_cast<int64_t*>(source)); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = static_cast<long_t>(*reinterpret_cast<int64_t*>(source));   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = static_cast<ulong_t>(*reinterpret_cast<int64_t*>(source));  break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = static_cast<float>(*reinterpret_cast<int64_t*>(source));    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = static_cast<double>(*reinterpret_cast<int64_t*>(source));   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( (int)type2 ).Throw();
          }
          break;
        }
        case TYPE_UINT64:
        {
          switch( type2 )
          {
            case TYPE_VOID_PTR: *reinterpret_cast<mem_t*>(out)    = (mem_t)(*reinterpret_cast<uint64_t*>(source));               break;
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = static_cast<int8_t>(*reinterpret_cast<uint64_t*>(source));   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = static_cast<uint8_t>(*reinterpret_cast<uint64_t*>(source));  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = static_cast<int16_t>(*reinterpret_cast<uint64_t*>(source));  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = static_cast<uint16_t>(*reinterpret_cast<uint64_t*>(source)); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = static_cast<int32_t>(*reinterpret_cast<uint64_t*>(source));  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = static_cast<uint32_t>(*reinterpret_cast<uint64_t*>(source)); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = static_cast<int64_t>(*reinterpret_cast<uint64_t*>(source));  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = static_cast<uint64_t>(*reinterpret_cast<uint64_t*>(source)); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = static_cast<long_t>(*reinterpret_cast<uint64_t*>(source));   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = static_cast<ulong_t>(*reinterpret_cast<uint64_t*>(source));  break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = static_cast<float>(*reinterpret_cast<uint64_t*>(source));    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = static_cast<double>(*reinterpret_cast<uint64_t*>(source));   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( (int)type2 ).Throw();
          }
          break;
        }
        case TYPE_LONG:
        {
          switch( type2 )
          {
            case TYPE_VOID_PTR: *reinterpret_cast<mem_t*>(out)    = (mem_t)(*reinterpret_cast<long_t*>(source));               break;
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = static_cast<int8_t>(*reinterpret_cast<long_t*>(source));   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = static_cast<uint8_t>(*reinterpret_cast<long_t*>(source));  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = static_cast<int16_t>(*reinterpret_cast<long_t*>(source));  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = static_cast<uint16_t>(*reinterpret_cast<long_t*>(source)); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = static_cast<int32_t>(*reinterpret_cast<long_t*>(source));  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = static_cast<uint32_t>(*reinterpret_cast<long_t*>(source)); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = static_cast<int64_t>(*reinterpret_cast<long_t*>(source));  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = static_cast<uint64_t>(*reinterpret_cast<long_t*>(source)); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = static_cast<long_t>(*reinterpret_cast<long_t*>(source));   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = static_cast<ulong_t>(*reinterpret_cast<long_t*>(source));  break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = static_cast<float>(*reinterpret_cast<long_t*>(source));    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = static_cast<double>(*reinterpret_cast<long_t*>(source));   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( (int)type2 ).Throw();
          }
          break;
        }
        case TYPE_ULONG:
        {
          switch( type2 )
          {
            case TYPE_VOID_PTR: *reinterpret_cast<mem_t*>(out)    = (mem_t)(*reinterpret_cast<ulong_t*>(source));               break;
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = static_cast<int8_t>(*reinterpret_cast<ulong_t*>(source));   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = static_cast<uint8_t>(*reinterpret_cast<ulong_t*>(source));  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = static_cast<int16_t>(*reinterpret_cast<ulong_t*>(source));  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = static_cast<uint16_t>(*reinterpret_cast<ulong_t*>(source)); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = static_cast<int32_t>(*reinterpret_cast<ulong_t*>(source));  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = static_cast<uint32_t>(*reinterpret_cast<ulong_t*>(source)); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = static_cast<int64_t>(*reinterpret_cast<ulong_t*>(source));  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = static_cast<uint64_t>(*reinterpret_cast<ulong_t*>(source)); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = static_cast<long_t>(*reinterpret_cast<ulong_t*>(source));   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = static_cast<ulong_t>(*reinterpret_cast<ulong_t*>(source));  break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = static_cast<float>(*reinterpret_cast<ulong_t*>(source));    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = static_cast<double>(*reinterpret_cast<ulong_t*>(source));   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( (int)type2 ).Throw();
          }
          break;
        }
        case TYPE_FLOAT:
        {
          switch( type2 )
          {
            case TYPE_VOID_PTR: Exception().Append( "Illegal conversion " ).Append( TYPE_NAME[type1] ).Append( " -> " ).Append( TYPE_NAME[type2] ).Throw();
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = static_cast<int8_t>(*reinterpret_cast<float*>(source));   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = static_cast<uint8_t>(*reinterpret_cast<float*>(source));  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = static_cast<int16_t>(*reinterpret_cast<float*>(source));  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = static_cast<uint16_t>(*reinterpret_cast<float*>(source)); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = static_cast<int32_t>(*reinterpret_cast<float*>(source));  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = static_cast<uint32_t>(*reinterpret_cast<float*>(source)); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = static_cast<int64_t>(*reinterpret_cast<float*>(source));  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = static_cast<uint64_t>(*reinterpret_cast<float*>(source)); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = static_cast<long_t>(*reinterpret_cast<float*>(source));   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = static_cast<ulong_t>(*reinterpret_cast<float*>(source));  break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = static_cast<float>(*reinterpret_cast<float*>(source));    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = static_cast<double>(*reinterpret_cast<float*>(source));   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( (int)type2 ).Throw();
          }
          break;
        }
        case TYPE_DOUBLE:
        {
          switch( type2 )
          {
            case TYPE_VOID_PTR: Exception().Append( "Illegal conversion " ).Append( TYPE_NAME[type1] ).Append( " -> " ).Append( TYPE_NAME[type2] ).Throw();
            case TYPE_INT8:     *reinterpret_cast<int8_t*>(out)   = static_cast<int8_t>(*reinterpret_cast<double*>(source));   break;
            case TYPE_UINT8:    *reinterpret_cast<uint8_t*>(out)  = static_cast<uint8_t>(*reinterpret_cast<double*>(source));  break;
            case TYPE_INT16:    *reinterpret_cast<int16_t*>(out)  = static_cast<int16_t>(*reinterpret_cast<double*>(source));  break;
            case TYPE_UINT16:   *reinterpret_cast<uint16_t*>(out) = static_cast<uint16_t>(*reinterpret_cast<double*>(source)); break;
            case TYPE_INT32:    *reinterpret_cast<int32_t*>(out)  = static_cast<int32_t>(*reinterpret_cast<double*>(source));  break;
            case TYPE_UINT32:   *reinterpret_cast<uint32_t*>(out) = static_cast<uint32_t>(*reinterpret_cast<double*>(source)); break;
            case TYPE_INT64:    *reinterpret_cast<int64_t*>(out)  = static_cast<int64_t>(*reinterpret_cast<double*>(source));  break;
            case TYPE_UINT64:   *reinterpret_cast<uint64_t*>(out) = static_cast<uint64_t>(*reinterpret_cast<double*>(source)); break;
            case TYPE_LONG:     *reinterpret_cast<long_t*>(out)   = static_cast<long_t>(*reinterpret_cast<double*>(source));   break;
            case TYPE_ULONG:    *reinterpret_cast<ulong_t*>(out)  = static_cast<ulong_t>(*reinterpret_cast<double*>(source));  break;
            case TYPE_FLOAT:    *reinterpret_cast<float*>(out)    = static_cast<float>(*reinterpret_cast<double*>(source));    break;
            case TYPE_DOUBLE:   *reinterpret_cast<double*>(out)   = static_cast<double>(*reinterpret_cast<double*>(source));   break;
            default: Exception().Append( "Unknown operand type " ).AppendHex( (int)type2 ).Throw();
          }
          break;
        }
        default: Exception().Append( "Unknown operand type " ).AppendHex( type1 ).Throw();
      }
    }
  };
}