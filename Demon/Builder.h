#pragma once

#include <stack>
#include "PreparedStatement_Variable.h"
#include "PreparedStatement_Expression.h"
#include "PreparedStatement_Block.h"

namespace Demon {
  class ExpressionBuilder {
    std::stack<std::shared_ptr<PreparedStatement_EXPRESSION>> expressionStatements;
  public:
    std::shared_ptr<PreparedStatement_EXPRESSION> StartExpression() {
      return expressionStatements.emplace( std::make_shared<PreparedStatement_EXPRESSION>() );
    }

    std::shared_ptr<PreparedStatement_EXPRESSION> GetExpression() {
      return expressionStatements.top();
    }

    std::shared_ptr<PreparedStatement_EXPRESSION> EndExpression() {
      std::shared_ptr<PreparedStatement_EXPRESSION> expressionStatement = GetExpression();
      expressionStatements.pop();
      expressionStatement->Resort();
      return expressionStatement;
    }

    ExpressionBuilder& Append( void* value ) {
      std::shared_ptr<PreparedStatement_CONSTANT> constStatement( std::make_shared<PreparedStatement_CONSTANT>( TYPE_VOID_PTR ) );
      constStatement->Constant.ULong = (ulong_t)value;
      GetExpression()->Append( std::move(constStatement) );
      return *this;
    }

    ExpressionBuilder& Append( int8_t value ) {
      std::shared_ptr<PreparedStatement_CONSTANT> constStatement( std::make_shared<PreparedStatement_CONSTANT>( TYPE_INT8 ) );
      constStatement->Constant.Int8 = value;
      GetExpression()->Append( std::move(constStatement) );
      return *this; 
    }

    ExpressionBuilder& Append( uint8_t value ) {
      std::shared_ptr<PreparedStatement_CONSTANT> constStatement( std::make_shared<PreparedStatement_CONSTANT>( TYPE_UINT8 ) );
      constStatement->Constant.UInt8 = value;
      GetExpression()->Append( std::move(constStatement) );
      return *this;
    }

    ExpressionBuilder& Append( int16_t value ) {
      std::shared_ptr<PreparedStatement_CONSTANT> constStatement( std::make_shared<PreparedStatement_CONSTANT>( TYPE_INT16 ) );
      constStatement->Constant.Int16 = value;
      GetExpression()->Append( std::move(constStatement) );
      return *this;
    }

    ExpressionBuilder& Append( uint16_t value ) {
      std::shared_ptr<PreparedStatement_CONSTANT> constStatement( std::make_shared<PreparedStatement_CONSTANT>( TYPE_UINT16 ) );
      constStatement->Constant.UInt16 = value;
      GetExpression()->Append( std::move(constStatement) );
      return *this;
    }

    ExpressionBuilder& Append( int32_t value ) {
      std::shared_ptr<PreparedStatement_CONSTANT> constStatement( std::make_shared<PreparedStatement_CONSTANT>( TYPE_INT32 ) );
      constStatement->Constant.Int32 = value;
      GetExpression()->Append( std::move(constStatement) );
      return *this;
    }

    ExpressionBuilder& Append( uint32_t value ) {
      std::shared_ptr<PreparedStatement_CONSTANT> constStatement( std::make_shared<PreparedStatement_CONSTANT>( TYPE_UINT32 ) );
      constStatement->Constant.UInt32 = value;
      GetExpression()->Append( std::move(constStatement) );
      return *this;
    }

    ExpressionBuilder& Append( int64_t value ) {
      std::shared_ptr<PreparedStatement_CONSTANT> constStatement( std::make_shared<PreparedStatement_CONSTANT>( TYPE_INT64 ) );
      constStatement->Constant.Int64 = value;
      GetExpression()->Append( std::move(constStatement) );
      return *this;
    }

    ExpressionBuilder& Append( uint64_t value ) {
      std::shared_ptr<PreparedStatement_CONSTANT> constStatement( std::make_shared<PreparedStatement_CONSTANT>( TYPE_UINT64 ) );
      constStatement->Constant.UInt64 = value;
      GetExpression()->Append( std::move(constStatement) );
      return *this;
    }

#ifndef _WIN64
    ExpressionBuilder& Append( long_t value ) {
      std::shared_ptr<PreparedStatement_CONSTANT> constStatement( std::make_shared<PreparedStatement_CONSTANT>( TYPE_LONG ) );
      constStatement->Constant.Long = value;
      GetExpression()->Append( std::move(constStatement) );
      return *this;
    }
    
    ExpressionBuilder& Append( ulong_t value ) {
      std::shared_ptr<PreparedStatement_CONSTANT> constStatement( std::make_shared<PreparedStatement_CONSTANT>( TYPE_ULONG ) );
      constStatement->Constant.ULong = value;
      GetExpression()->Append( std::move(constStatement) );
      return *this;
    }
#endif

    ExpressionBuilder& Append( float value ) {
      std::shared_ptr<PreparedStatement_CONSTANT> constStatement( std::make_shared<PreparedStatement_CONSTANT>( TYPE_FLOAT ) );
      constStatement->Constant.Float = value;
      GetExpression()->Append( std::move(constStatement) );
      return *this;
    }

    ExpressionBuilder& Append( double value ) {
      std::shared_ptr<PreparedStatement_CONSTANT> constStatement( std::make_shared<PreparedStatement_CONSTANT>( TYPE_DOUBLE ) );
      constStatement->Constant.Double = value;
      GetExpression()->Append( std::move(constStatement) );
      return *this;
    }

    ExpressionBuilder& Append( OPERATOR_UID operatorUID ) {
      std::shared_ptr<PreparedStatement_OPERATOR> operatorStatement( std::make_shared<PreparedStatement_OPERATOR>( operatorUID ) );
      GetExpression()->Append( std::move(operatorStatement) );
      return *this;
    }

    ExpressionBuilder& Append( std::shared_ptr<PreparedStatement> statement ) {
      GetExpression()->Append( std::move(statement) );
      return *this;
    }
  };


  class CodeBuilder : public ExpressionBuilder {
  public: // DELETE ME
    Code code;
    PreparedStatement_BLOCK block;
  public:
    CodeBuilder() { }

    std::shared_ptr<PreparedStatement_EXPRESSION> EndExpression() {
      std::shared_ptr<PreparedStatement_EXPRESSION> expressionStatement = ExpressionBuilder::EndExpression();
      block.Append( expressionStatement );
      return expressionStatement;
    }

    std::shared_ptr<PreparedStatement> GetBackStatement() {
      return GetExpression()->UnsortedStatements.back();
    }

    std::shared_ptr<PreparedStatement_EXPRESSION> GetTopExpression() {
      return block.Expressions[block.Expressions.size() - 2];
    }

    Code& Compile() {
      block.Compile( code );
      return code;
    }
  };
}