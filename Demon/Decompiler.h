﻿#pragma once

#include <string>
#include <sstream>
#include <iomanip>
#include "PreparedStatement_Block.h"
#include "Terminal.h"

namespace Demon {
  class Decompiler {
  protected:
    Code& code;
    uint64_t position;

    void ReadCode( ulong_t start, ulong_t end, std::wstringstream* stream ) {
      Terminal::begin( stream ).endline();
      for( ulong_t i = start; i < end; i++ ) {
        Terminal::begin( stream ).setForeground( LGRAY )
          .out( std::setw( 2 ) ).out( std::setfill( L'0' ) ).out( std::hex )
          .out( code[i] )
          .out( std::dec )
          .out( " " ).resetColors();
      }
    }

    void ReadValue( uint8_t typeUID, std::wstringstream& stream ) {
      switch( typeUID )
      {
        case TYPE_VOID_PTR: stream <<      code.GetValue<void*>   ( position ); break;
        case TYPE_INT8:     stream << (int)code.GetValue<int8_t>  ( position ); break;
        case TYPE_UINT8:    stream <<      code.GetValue<uint8_t> ( position ); break;
        case TYPE_INT16:    stream <<      code.GetValue<int16_t> ( position ); break;
        case TYPE_UINT16:   stream <<      code.GetValue<uint16_t>( position ); break;
        case TYPE_INT32:    stream <<      code.GetValue<int32_t> ( position ); break;
        case TYPE_UINT32:   stream <<      code.GetValue<uint32_t>( position ); break;
        case TYPE_INT64:    stream <<      code.GetValue<int64_t> ( position ); break;
        case TYPE_UINT64:   stream <<      code.GetValue<uint64_t>( position ); break;
        case TYPE_LONG:     stream <<      code.GetValue<long_t>  ( position ); break;
        case TYPE_ULONG:    stream <<      code.GetValue<ulong_t> ( position ); break;
        case TYPE_FLOAT:    stream <<      code.GetValue<float>   ( position ); break;
        case TYPE_DOUBLE:   stream <<      code.GetValue<double>  ( position ); break;
      }
    }

    void ReadOperand( uint8_t type, std::wstringstream* stream ) {
      uint8_t operandUID = code.GetValue<uint8_t>( position );
      switch( operandUID )
      {
        case OPERAND_REG:
        {
          std::wstring resisterName = GetRegisterName( code.GetValue<int8_t>( position ) );
          Terminal::begin( stream )
            .setForeground( LGRAY ).out( "reg" )
            .setForeground( GRAY  ).out( "[" )
            .setForeground( WHITE ).out( resisterName )
            .setForeground( GRAY  ).out( "]" );

          break;
        }

        case OPERAND_IMM:
        {
          std::wstringstream value;
          ReadValue( type, value );
          Terminal::begin( stream ).setForeground( GRAY ).out( value.str() );
          break;
        }

        case OPERAND_REG_MEM:
        {
          std::wstring resisterName = GetRegisterName( code.GetValue<int8_t>( position ) );
          Terminal::begin( stream )
            .setForeground( WHITE ).out( "[" )
            .setForeground( LGRAY ).out( "reg" )
            .setForeground( GRAY  ).out( "[" )
            .setForeground( WHITE ).out( resisterName )
            .setForeground( GRAY  ).out( "]" )
            .setForeground( WHITE ).out( "]" );
          break;
        }

        case OPERAND_IMM_MEM:
        {
          std::wstringstream value;
          ReadValue( TYPE_ULONG, value );

          Terminal::begin( stream )
                   .setForeground( WHITE   ).out( "[" )
                   .setForeground( LYELLOW ).out( value.str() )
                   .setForeground( WHITE   ).out( "]" );
          break;
        }

        case OPERAND_REG_REG_MEM:
        {
          std::wstring registerName1 = GetRegisterName( code.GetValue<int8_t>( position ) );
          std::wstring registerName2 = GetRegisterName( code.GetValue<int8_t>( position ) );
          Terminal::begin( stream )
            .setForeground( WHITE   ).out( "[" )
            .setForeground( LGRAY   ).out( "reg" )
            .setForeground( GRAY    ).out( "[" )
            .setForeground( WHITE   ).out( registerName1 )
            .setForeground( GRAY    ).out( "]" )
            .setForeground( LYELLOW ).out( "+" )
            .setForeground( LGRAY   ).out( "reg" )
            .setForeground( GRAY    ).out( "[" )
            .setForeground( WHITE   ).out( registerName2 )
            .setForeground( GRAY    ).out( "]" )
            .setForeground( WHITE   ).out( "]" );
          break;
        }

        case OPERAND_REG_IMM_MEM:
        {
          std::wstring registerName = GetRegisterName( code.GetValue<int8_t>( position ) );
          std::wstringstream value;
          ReadValue( TYPE_ULONG, value );
          Terminal::begin( stream )
            .setForeground( WHITE   ).out( "[" )
            .setForeground( LGRAY   ).out( "reg" )
            .setForeground( GRAY    ).out( "[" )
            .setForeground( WHITE   ).out( registerName )
            .setForeground( GRAY    ).out( "]" )
            .setForeground( LYELLOW ).out( "+" )
            .setForeground( LYELLOW ).out( value.str() )
            .setForeground( WHITE   ).out( "]" );
        }
      }
    }
  public:
    bool OutputBytecode = false;
    int LinebreakCount = 2;

    Decompiler( Code& code ) : code( code ), position(0) { }

    bool Next( std::wstringstream* stream ) {
      ulong_t start = position;

      if( position >= code.Length() )
        return false;

      uint8_t instructionUID = code.GetValue<uint8_t>( position );
      Terminal::begin( stream )
        .setForeground( GRAY  ).out( (void*)position ).tabulate()
        .setForeground( WHITE ).out( INSTRUCTION_NAME[instructionUID] );

      switch( instructionUID )
      {
        case INSTRUCTION_RET:
          break;

        case INSTRUCTION_MOV:
        case INSTRUCTION_ADD:
        case INSTRUCTION_SUB:
        case INSTRUCTION_MUL:
        case INSTRUCTION_DIV:
        case INSTRUCTION_MOD:
        case INSTRUCTION_LSHIFT:
        case INSTRUCTION_RSHIFT:
        case INSTRUCTION_BIT_AND:
        case INSTRUCTION_BIT_XOR:
        case INSTRUCTION_BIT_OR:
        {
          uint8_t typeUID = code.GetValue<uint8_t>( position );
          const wchar_t* typeName = TYPE_NAME[typeUID];

          Terminal::begin( stream )
            .setForeground( YELLOW ).out( "<" )
            .setForeground( LCYAN  ).out( typeName )
            .setForeground( YELLOW ).out( ">" )
            .resetColors().tabulate( 2 );

          Terminal::begin( stream )
            .setForeground( LRED ).out( "lval" )
            .setForeground( RED  ).out( ":" )
            .resetColors();
          ReadOperand( typeUID, stream );
          Terminal::begin( stream ).resetColors();

          Terminal::begin( stream )
            .setForeground( LRED ).out( "   rval" )
            .setForeground( RED ).out( ":" )
            .resetColors();
          ReadOperand( typeUID, stream );
          Terminal::begin( stream ).resetColors();

          Terminal::begin( stream )
            .setForeground( LRED ).out( "   out" )
            .setForeground( RED ).out( ":" ).resetColors();
          ReadOperand( typeUID, stream );
          Terminal::begin( stream ).resetColors();
          break;
        }

        case INSTRUCTION_COMPL:
        case INSTRUCTION_NOT:
        case INSTRUCTION_NEG:
        case INSTRUCTION_INC:
        case INSTRUCTION_DEC:
        {
          uint8_t typeUID = code.GetValue<uint8_t>( position );
          Terminal::begin( stream )
            .setForeground( YELLOW ).out( "<" )
            .setForeground( LCYAN  ).out( TYPE_NAME[typeUID] )
            .setForeground( YELLOW ).out( ">" )
            .resetColors().tabulate( 2 );

          Terminal::begin( stream )
            .setForeground( LRED )
            .setForeground( LRED ).out( "lval" )
            .setForeground( RED ).out( ":" )
            .resetColors();
          ReadOperand( typeUID, stream );
          Terminal::begin( stream ).resetColors();

          Terminal::begin( stream )
            .setForeground( LRED ).out( "   out" )
            .setForeground( RED ).out( ":" )
            .resetColors();
          ReadOperand( typeUID, stream );
          Terminal::begin( stream ).resetColors();
          break;
        }

        case INSTRUCTION_CAST:
        {
          uint8_t typeUID1 = code.GetValue<uint8_t>( position );
          uint8_t typeUID2 = code.GetValue<uint8_t>( position );

          Terminal::begin( stream )
            .setForeground( YELLOW ).out( "<" )
            .setForeground( LCYAN  ).out( TYPE_NAME[typeUID1] )
            .setForeground( YELLOW ).out( "," )
            .setForeground( LCYAN  ).out( TYPE_NAME[typeUID2] )
            .setForeground( YELLOW ).out( ">" )
            .resetColors().tabulate( 1 );

          Terminal::begin( stream )
            .setForeground( LRED ).out( "lval" )
            .setForeground( RED  ).out( ":" )
            .resetColors();
          ReadOperand( typeUID1, stream );

          Terminal::begin( stream )
            .setForeground( LRED ).out( "   out" )
            .setForeground( RED ).out( ":" ).resetColors();
          ReadOperand( typeUID2, stream );
          break;
        }
      }

      if( OutputBytecode )
        ReadCode( start, position, stream );

      if( !stream )
        Terminal::begin().endline( LinebreakCount );

      return true;
    }
  };
}