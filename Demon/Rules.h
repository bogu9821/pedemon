#pragma once

#include "Constants.h"

namespace Demon {
  const ulong_t OPERATOR_PRIORITY[OPERATOR_UID_MAX] = {
    0,  // return
    0,  // A()
    0,  // A[B]
    0,  // ++A
    0,  // --A
    1,  // <TYPE>A
    1,  // (TYPE)A
    2,  // ~A
    2,  // !A
    2,  // -A
    2,  // +A
    2,  // *A
    14, // A++
    14, // A--
    3,  // A*B
    3,  // A/B
    3,  // A%B
    4,  // A+B
    4,  // A-B
    5,  // A<<B
    5,  // A>>B
    6,  // A<B
    6,  // A>B
    6,  // A<=B
    6,  // A>=B
    7,  // A==B
    7,  // A!=B
    8,  // A&B
    9,  // A^B
    10, // A|B
    11, // A&&B
    12, // A||B
    13, // A=B
    13, // A*=B
    13, // A/=B
    13, // A%=B
    13, // A+=B
    13, // A-=B
    13, // A<<=B
    13, // A>>=B
    13, // A&=B
    13, // A^=B
    13, // A|=B
  };


  const ulong_t OPERATOR_DIRECTION[OPERATOR_UID_MAX] = {
    0,                                                                                     // return
                                                           SOURCE_FLAG_RETURN_DESTINATION, // A()
                               SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_SOURCE,      // A[B]
                               SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_SOURCE,      // ++A
                               SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_SOURCE,      // --A
                               SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_SOURCE,      // <TYPE>A
                               SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // (TYPE)A
                               SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // ~A
                               SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // !A
                               SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // -A
                               SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // +A
                               SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // *A
    SOURCE_FLAG_OPERAND_LEFT                             | SOURCE_FLAG_RETURN_SOURCE,      // A++
    SOURCE_FLAG_OPERAND_LEFT                             | SOURCE_FLAG_RETURN_SOURCE,      // A--
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // A*B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // A/B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // A%B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // A+B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // A-B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // A<<B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // A>>B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // A<B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // A>B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // A<=B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // A>=B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // A==B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // A!=B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // A&B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // A^B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // A|B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // A&&B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_DESTINATION, // A||B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_SOURCE,      // A=B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_SOURCE,      // A*=B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_SOURCE,      // A/=B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_SOURCE,      // A%=B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_SOURCE,      // A+=B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_SOURCE,      // A-=B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_SOURCE,      // A<<=B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_SOURCE,      // A>>=B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_SOURCE,      // A&=B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_SOURCE,      // A^=B
    SOURCE_FLAG_OPERAND_LEFT | SOURCE_FLAG_OPERAND_RIGHT | SOURCE_FLAG_RETURN_SOURCE       // A|=B
  };


  const bool OPERATOR_AUTOCAST[OPERATOR_UID_MAX] = {
    false, // RET
    false, // A()
    false, // A[B]
    false, // ++A
    false, // --A
    false, // <TYPE>A
    false, // (TYPE)A
    false, // ~A
    false, // !A
    false, // -A
    false, // +A
    false, // *A
    false, // A++
    false, // A--
    true,  // A*B
    true,  // A/B
    true,  // A%B
    true,  // A+B
    true,  // A-B
    true,  // A<<B
    true,  // A>>B
    true,  // A<B
    true,  // A>B
    true,  // A<=B
    true,  // A>=B
    true,  // A==B
    true,  // A!=B
    true,  // A&B
    true,  // A^B
    true,  // A|B
    false, // A&&B
    false, // A||B
    false, // A=B
    true,  // A*=B
    true,  // A/=B
    true,  // A%=B
    true,  // A+=B
    true,  // A-=B
    true,  // A<<=B
    true,  // A>>=B
    true,  // A&=B
    true,  // A^=B
    true,  // A|=B
  };
}