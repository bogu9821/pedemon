#pragma once

#include <string>
#include "PreparedStatement.h"

namespace Demon {
  class PreparedStatement_LABEL : public PreparedStatement {
  public:
    std::wstring Name;
    ulong_t Position = -1;
  };
}