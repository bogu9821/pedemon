﻿#pragma once

#include <string>
#include <unordered_map>
#include <sstream>
#include "Tokenizer.h"
#include "Builder.h"

#undef min
#undef max

namespace Demon {
  enum TOKEN_GROUP_UID {
    TOKEN_GROUP_INVALID = -1,
    TOKEN_GROUP_COMMENT_BLOCK,
    TOKEN_GROUP_COMMENT_LINE,
    TOKEN_GROUP_STRING,
    TOKEN_GROUP_CHAR,
    TOKEN_GROUP_EXPRESSION,
    TOKEN_GROUP_INDEX,
    TOKEN_GROUP_REINTERPRET_CAST,
    TOKEN_GROUP_OPERATOR,
    TOKEN_GROUP_KEYWORD,
    TOKEN_GROUP_TYPE,
    TOKEN_GROUP_SPACE,
    TOKEN_GROUP_NEW_LINE,
    TOKEN_GROUP_BIN_DIGIT,
    TOKEN_GROUP_OCT_DIGIT,
    TOKEN_GROUP_HEX_DIGIT,
    TOKEN_GROUP_DIGIT,
    TOKEN_GROUP_DEFINITION,
    TOKEN_GROUP_UID_MAX
  };

  class Parser {
    struct Iterator {
      const wchar_t* Cursor;
      const wchar_t* End;
      int ContentID;
      std::wstring Content;

      Iterator( const std::wstring& str ) {
        Cursor = str.c_str();
        End = Cursor + str.length();
      }

      bool Next( Tokenizer& tokenizer ) {
        bool ok = Cursor < End;
        Content = tokenizer.Next( Cursor, ContentID );
        return ok;
      }

      operator bool() {
        return Cursor < End;
      }

      bool operator == ( const wchar_t* value ) {
        return Content == value;
      }

      bool operator == ( const std::wstring& value ) {
        return Content == value;
      }

      bool operator == ( int value ) {
        return ContentID == value;
      }

      void operator = ( Iterator& it ) {
        Cursor = it.Cursor;
        End = it.End;
        ContentID = it.ContentID;
        Content = it.Content;
      }
    };

  public: // DELETE ME
    Tokenizer tokenizer;
    CodeBuilder builder;
    static std::unordered_map<std::wstring, std::shared_ptr<PreparedStatement_VARIABLE>> variables; // DELETE ME
  public:
    Parser() : tokenizer(
      {
      new TokenGroupDelimiter( TOKEN_GROUP_COMMENT_BLOCK,    L"<--", L"-->" ),
      new TokenGroupDelimiter( TOKEN_GROUP_COMMENT_LINE,     L"--",  L"" ),
      new TokenGroupDelimiter( TOKEN_GROUP_STRING,           L"\"",  L"\"" ),
      new TokenGroupDelimiter( TOKEN_GROUP_CHAR,             L"\'",  L"\'" ),
      new TokenGroupDelimiter( TOKEN_GROUP_INDEX,            L"[",   L"]" ),
      new TokenGroupDelimiter( TOKEN_GROUP_REINTERPRET_CAST, L"<",   L">" ),

      new TokenGroupEnum( TOKEN_GROUP_OPERATOR,
        {
        L"++",  L"--",  L"~",   L"!",   L"*",   L"/",   L"%",   L"+",
        L"-",   L"<<",  L">>",  L"<",   L">",   L"<=",  L">=",  L"==",
        L"!=",  L"&",   L"^",   L"|",   L"&&",  L"||",  L"=",   L"*=",
        L"/=",  L"%=",  L"+=",  L"-=",  L"<<=", L">>=", L"&=",  L"^=",
        L"|=",  L"(",   L")"
        } ),

      new TokenGroupEnum( TOKEN_GROUP_KEYWORD,
        {
        L"return",
        L"if",
        L"else",
        L"for",
        L"while",
        L"continue",
        L"break",
        L"struct"
        } ),

      new TokenGroupEnum( TOKEN_GROUP_TYPE,
        {
        L"int8",   L"char",
        L"uint8",  L"unsigned char",  L"byte",
        L"int16",  L"short", L"wchar",
        L"uint16", L"unsigned short",
        L"int32",  L"int",
        L"uint32", L"unsigned int",
        L"int64",  L"long long",
        L"uint64", L"unsigned long long",
        L"long",
        L"ulong",  L"unsigned long",
        L"float",
        L"double",
        L"void"
        } ),

      new TokenGroupSequence( TOKEN_GROUP_SPACE,      L" \t" ),
      new TokenGroupSequence( TOKEN_GROUP_NEW_LINE,   L"\n\r" ),
      new TokenGroupPrefix(   TOKEN_GROUP_OCT_DIGIT,  L"0",  L"012345678", L"uil" ),
      new TokenGroupPrefix(   TOKEN_GROUP_BIN_DIGIT,  L"0b", L"0123468", L"uil" ),
      new TokenGroupPrefix(   TOKEN_GROUP_HEX_DIGIT,  L"0x", L"0123456789abcdefABCDEF", L"uil" ),
      new TokenGroupSequence( TOKEN_GROUP_DIGIT,      L"0123456789.", L"fuil" ),
      new TokenGroupSequence( TOKEN_GROUP_DEFINITION, L"abcdefghijklmnopurstuvwxyzABCDEFGHIJKLMNOPURSTUVWXYZ_", L"1234567890." ),

      } ) { }

  public:
    std::shared_ptr<PreparedStatement_EXPRESSION> Parse( std::wstring script ) {
      builder.StartExpression();
      {
        Iterator it = script;
        while( it.Next( tokenizer ) ) {
          switch( it.ContentID )
          {
            case TOKEN_GROUP_INVALID:
            case TOKEN_GROUP_COMMENT_BLOCK:
            case TOKEN_GROUP_COMMENT_LINE:
            case TOKEN_GROUP_SPACE:
              break;

            case TOKEN_GROUP_DIGIT:
              parseDigit( it );
              break;

            case TOKEN_GROUP_HEX_DIGIT:
              parseHexadecimalDigit( it );
              break;

            case TOKEN_GROUP_OCT_DIGIT:
              parseOctalDigit( it );
              break;

            case TOKEN_GROUP_BIN_DIGIT:
              parseBinaryDigit( it );
              break;

            case TOKEN_GROUP_OPERATOR:
              resolveOperatorSemantic( it );
              break;

            case TOKEN_GROUP_DEFINITION:
              parseDefinition( it );
              break;

            case TOKEN_GROUP_TYPE:
              declareBasicVariable( it );
              break;
          }
        }
      }
      return builder.EndExpression();
    }


    std::shared_ptr<PreparedStatement_EXPRESSION> GetResultExpression() {
      return builder.GetTopExpression();
    }


    Code& Compile() {
      builder.StartExpression();
      builder.Append( OPERATOR_RET );
      builder.EndExpression();
      return builder.Compile();
    }

  protected:
    //
    // Operators
    //
#pragma region operators
    void resolveOperatorSemantic( Iterator& it ) {
      if( it == L"(" ) {
        Iterator itTmp( it );
        if( itTmp.Next( tokenizer ) ) {
          if( resolveTypeName( itTmp.Content ) != TYPE_UID_MAX ) {
            it = itTmp;
            parseStaticCast( it );
            return;
          }
        }

        builder.StartExpression();
      }
      else if( it == L")" ) {
        auto psExpression = builder.EndExpression();
        builder.Append( psExpression );
      }
      else {
        parseOperator( it.Content );
      }
    }


    void parseStaticCast( Iterator& it ) {
      TYPE_UID typeUID = resolveTypeName( it.Content );
      std::wstring typeName = it.Content;
      int pointer = 0;
      while( it.Next( tokenizer ) ) {
        if( it.ContentID != TOKEN_GROUP_OPERATOR )
          break;

        if( it == L"*" ) {
          pointer++;
          continue;
        }

        if( it == L")" ) {
          if( typeUID < TYPE_STRUCT ) {
            builder.Append( OPERATOR_CAST_STATIC );
            auto ps = builder.GetBackStatement();
            auto psStaticCast = std::static_pointer_cast<PreparedStatement_OPERATOR>(ps);
            std::shared_ptr<SourceContext> source = psStaticCast->CreateSourceContext( typeUID );
            source->Pointer = pointer;
          }
          else {
            // TODO: struct
          }

          return;
        }
      }

      Exception().Append( "expected ')'" ).Throw();
    }


    void parseOperator( std::wstring& content ) {
      if( content == L"++" ) {
        std::shared_ptr<PreparedStatement> statement = builder.GetExpression()->Back();
        if( statement ) {
          const auto statementOperator = dynamic_cast<PreparedStatement_OPERATOR*>(statement.get());
          if( !statementOperator ) {
            builder.Append( OPERATOR_INC_POST );
            return;
          }
        }

        builder.Append( OPERATOR_INC_PRE );
      }
      else if( content == L"--" ) {
        std::shared_ptr<PreparedStatement> statement = builder.GetExpression()->Back();
        if( statement ) {
          const auto statementOperator = dynamic_cast<PreparedStatement_OPERATOR*>(statement.get());
          if( !statementOperator ) {
            builder.Append( OPERATOR_DEC_POST );
            return;
          }
        }

        builder.Append( OPERATOR_DEC_PRE );
      }
      else if( content == L"+" ) {
        std::shared_ptr<PreparedStatement> statement = builder.GetExpression()->Back();
        const auto statementOperator = dynamic_cast<PreparedStatement_OPERATOR*>(statement.get());
        if( !statement || statementOperator ) {
          builder.Append( OPERATOR_POS );
          return;
        }

        builder.Append( OPERATOR_ADD );
      }
      else if( content == L"-" ) {
        std::shared_ptr<PreparedStatement> statement = builder.GetExpression()->Back();
        const auto statementOperator = dynamic_cast<PreparedStatement_OPERATOR*>(statement.get());
        if( !statement || statementOperator ) {
          builder.Append( OPERATOR_NEG );
          return;
        }

        builder.Append( OPERATOR_SUB );
      }
      else if( content == L"*" ) {
        std::shared_ptr<PreparedStatement> statement = builder.GetExpression()->Back();
        const auto statementOperator = dynamic_cast<PreparedStatement_OPERATOR*>(statement.get());
        if( !statement || statementOperator ) {
          builder.Append( OPERATOR_EXT );
          return;
        }

        builder.Append( OPERATOR_MUL );
      }
      else if( content == L"~"   ) builder.Append( OPERATOR_COMPL );
      else if( content == L"!"   ) builder.Append( OPERATOR_NOT );
      else if( content == L"/"   ) builder.Append( OPERATOR_DIV );
      else if( content == L"%"   ) builder.Append( OPERATOR_MOD );
      else if( content == L"<<"  ) builder.Append( OPERATOR_LSHIFT );
      else if( content == L">>"  ) builder.Append( OPERATOR_RSHIFT );
      else if( content == L"<"   ) builder.Append( OPERATOR_LESS );
      else if( content == L">"   ) builder.Append( OPERATOR_GREATER );
      else if( content == L"<="  ) builder.Append( OPERATOR_LESS_OR_EQUAL );
      else if( content == L">="  ) builder.Append( OPERATOR_GREATER_OR_EQUAL );
      else if( content == L"=="  ) builder.Append( OPERATOR_EQUAL );
      else if( content == L"!="  ) builder.Append( OPERATOR_NOT_EQUAL );
      else if( content == L"&"   ) builder.Append( OPERATOR_BIT_AND );
      else if( content == L"^"   ) builder.Append( OPERATOR_BIT_XOR );
      else if( content == L"|"   ) builder.Append( OPERATOR_BIT_OR );
      else if( content == L"&&"  ) builder.Append( OPERATOR_AND );
      else if( content == L"||"  ) builder.Append( OPERATOR_OR );
      else if( content == L"="   ) builder.Append( OPERATOR_ASSIGN );
      else if( content == L"*="  ) builder.Append( OPERATOR_MUL_ASSIGN );
      else if( content == L"/="  ) builder.Append( OPERATOR_DIV_ASSIGN );
      else if( content == L"%="  ) builder.Append( OPERATOR_MOD_ASSIGN );
      else if( content == L"+="  ) builder.Append( OPERATOR_ADD_ASSIGN );
      else if( content == L"-="  ) builder.Append( OPERATOR_SUB_ASSIGN );
      else if( content == L"<<=" ) builder.Append( OPERATOR_LSHIFT_ASSIGN );
      else if( content == L">>=" ) builder.Append( OPERATOR_RSHIFT_ASSIGN );
      else if( content == L"&="  ) builder.Append( OPERATOR_BIT_AND_ASSIGN );
      else if( content == L"^="  ) builder.Append( OPERATOR_BIT_XOR_ASSIGN );
      else if( content == L"|="  ) builder.Append( OPERATOR_BIT_OR_ASSIGN );
      else Exception().Append( "Unsupported operator in this context: " ).Append( content ).Throw();
    }
#pragma endregion


    //
    // Digits
    //
#pragma region digits
    TYPE_UID resolveMinimalIntegerType( Digit digit ) {
      if( digit.Int64 >= std::numeric_limits<int8_t>::min() && digit.UInt64 <= std::numeric_limits<int8_t>::max() )
        return TYPE_INT8;
      if( digit.UInt64 <= std::numeric_limits<uint8_t>::max() )
        return TYPE_UINT8;

      if( digit.Int64 >= std::numeric_limits<int16_t>::min() && digit.Int64 <= std::numeric_limits<int16_t>::max() )
        return TYPE_INT16;
      if( digit.UInt64 <= std::numeric_limits<uint16_t>::max() )
        return TYPE_UINT16;

      if( digit.Int64 >= std::numeric_limits<int32_t>::min() && digit.Int64 <= std::numeric_limits<int32_t>::max() )
        return TYPE_INT32;
      if( digit.UInt64 <= std::numeric_limits<uint32_t>::max() )
        return TYPE_UINT32;

      if( std::cmp_less_equal(digit.UInt64, std::numeric_limits<int64_t>::max()) )
        return TYPE_INT64;
      
      return TYPE_UINT64;
    }


    bool eraseDigitLiteral( std::wstring& content, const std::wstring& literal ) {
      if( content.ends_with( literal ) ) {
        content = content.substr( 0, content.length() - literal.length() );
        return true;
      }

      return false;
    }


    TYPE_UID resolveDigitType( std::wstring& content ) {
      TYPE_UID typeUID = resolveIntegerType( content );
      if( typeUID != TYPE_UID_MAX )
        return typeUID;

      if( eraseDigitLiteral( content, L"f"   ) ) return TYPE_FLOAT;
      if( content.find( L'.' ) != -1 )           return TYPE_DOUBLE;

      return TYPE_UID_MAX;
    }


    TYPE_UID resolveIntegerType( std::wstring& content ) {
      if( eraseDigitLiteral( content, L"i8"  ) ) return TYPE_INT8;
      if( eraseDigitLiteral( content, L"u8"  ) ) return TYPE_UINT8;
      if( eraseDigitLiteral( content, L"i16" ) ) return TYPE_INT16;
      if( eraseDigitLiteral( content, L"u16" ) ) return TYPE_UINT16;
      if( eraseDigitLiteral( content, L"i32" ) ) return TYPE_INT32;
      if( eraseDigitLiteral( content, L"u"   ) ) return TYPE_UINT32;
      if( eraseDigitLiteral( content, L"u32" ) ) return TYPE_UINT32;
      if( eraseDigitLiteral( content, L"i64" ) ) return TYPE_INT64;
      if( eraseDigitLiteral( content, L"u64" ) ) return TYPE_UINT64;
      if( eraseDigitLiteral( content, L"l"   ) ) return TYPE_LONG;
      if( eraseDigitLiteral( content, L"ul"  ) ) return TYPE_ULONG;

      return TYPE_UID_MAX;
    }


    void parseDigit( Iterator& it ) {
      Digit digit;
      std::wstringstream converter;
      converter << it.Content;

      switch( resolveDigitType( it.Content ) )
      {
      case Demon::TYPE_FLOAT:
        converter >> digit.Float;
        builder.Append( digit.Float );
        break;
      case Demon::TYPE_DOUBLE:
        converter >> digit.Double;
        builder.Append( digit.Double );
        break;
      default:
        parseDecimalDigit( it );
        break;
      }
    }


    void parseDecimalDigit( Iterator& it ) {
      TYPE_UID preferredTypeUID = resolveDigitType( it.Content );

      Digit digit;
      std::wstringstream converter;
      converter << it.Content;
      converter >> digit.Int64;
      appendIntegerDigit( it, preferredTypeUID, digit );
    }


    void parseHexadecimalDigit( Iterator& it ) {
      TYPE_UID preferredTypeUID = resolveIntegerType( it.Content );

      Digit digit;
      std::wstringstream converter;
      converter << it.Content;
      converter >> std::hex >> digit.Int64 >> std::dec;
      appendIntegerDigit( it, preferredTypeUID, digit );
    }


    void parseOctalDigit( Iterator& it ) {
      TYPE_UID preferredTypeUID = resolveDigitType( it.Content );

      Digit digit;
      std::wstringstream converter;
      converter << it.Content;
      converter >> std::oct >> digit.Int64 >> std::dec;
      appendIntegerDigit( it, preferredTypeUID, digit );
    }


    void parseBinaryDigit( Iterator& it ) {
      TYPE_UID preferredTypeUID = resolveDigitType( it.Content );

      Digit digit;
      digit.UInt64 = 0;
      for( wchar_t wc : it.Content ) {
        digit.UInt64 <<= 1;
        if( wc == L'1' )
          digit.UInt64 |= 1ull;
      }

      appendIntegerDigit( it, preferredTypeUID, digit );
    }


    void appendIntegerDigit( Iterator& it, TYPE_UID preferredTypeUID, Digit digit ) {
      if( preferredTypeUID != TYPE_UID_MAX ) {
        TYPE_UID minimalTypeUID = resolveMinimalIntegerType( digit );
        if( TYPE_SIZE[preferredTypeUID] < TYPE_SIZE[minimalTypeUID] ) {
          Terminal::begin()
            .setForeground( LRED    ).out( "] " )
            .setForeground( LYELLOW ).out( "WARNING: " )
            .setForeground( GRAY    ).out( "information loss is possible (" )
            .setForeground( LCYAN   ).out( digit.Int64 )
            .setForeground( GRAY    ).out( ") " )
            .setForeground( LRED    ).out( TYPE_NAME[minimalTypeUID] )
            .setForeground( LYELLOW ).out( " -> " )
            .setForeground( LRED    ).out( TYPE_NAME[preferredTypeUID] )
            .setForeground( LGRAY   ).out( " [content: " )
            .setForeground( LGRAY   ).out( it.Content )
            .setForeground( LGRAY   ).out( "]" )
            .resetColors().endline();
        }
        
        switch( preferredTypeUID )
        {
          case Demon::TYPE_INT8:   builder.Append( digit.Int8   ); break;
          case Demon::TYPE_UINT8:  builder.Append( digit.UInt8  ); break;
          case Demon::TYPE_INT16:  builder.Append( digit.Int16  ); break;
          case Demon::TYPE_UINT16: builder.Append( digit.UInt16 ); break;
          case Demon::TYPE_INT32:  builder.Append( digit.Int32  ); break;
          case Demon::TYPE_UINT32: builder.Append( digit.UInt32 ); break;
          case Demon::TYPE_INT64:  builder.Append( digit.Int64  ); break;
          case Demon::TYPE_UINT64: builder.Append( digit.UInt64 ); break;
          case Demon::TYPE_LONG:   builder.Append( digit.Long   ); break;
          case Demon::TYPE_ULONG:  builder.Append( digit.ULong  ); break;
        }
      }
      else if( std::cmp_greater( digit.UInt64, std::numeric_limits<int64_t>::max()) )
        builder.Append( digit.UInt64 );
      else if( digit.UInt64 > std::numeric_limits<uint32_t>::max() )
        builder.Append( digit.Int64 );
      else if( digit.UInt64 > std::numeric_limits<int32_t>::max() )
        builder.Append( digit.UInt32 );
      else
        builder.Append( digit.Int32 );
    }
#pragma endregion


    //
    // Definition
    //
#pragma region definition
    std::shared_ptr<PreparedStatement_VARIABLE> findVariable( const std::wstring& name ) {
      if( variables.find( name ) == variables.end() )
        return nullptr;

      return variables[name];
    }


    TYPE_UID resolveTypeName( const std::wstring& content ) {
      if( content == L"void*"  ) return TYPE_VOID_PTR;
      if( content == L"int8"   ) return TYPE_INT8;
      if( content == L"char"   ) return TYPE_INT8;
      if( content == L"uint8"  ) return TYPE_UINT8;
      if( content == L"byte"   ) return TYPE_UINT8;
      if( content == L"int16"  ) return TYPE_INT16;
      if( content == L"wchar"  ) return TYPE_INT16;
      if( content == L"uint16" ) return TYPE_UINT16;
      if( content == L"int32"  ) return TYPE_INT32;
      if( content == L"int"    ) return TYPE_INT32;
      if( content == L"uint32" ) return TYPE_UINT32;
      if( content == L"uint"   ) return TYPE_UINT32;
      if( content == L"int64"  ) return TYPE_INT64;
      if( content == L"uint64" ) return TYPE_UINT64;
      if( content == L"float"  ) return TYPE_FLOAT;
      if( content == L"double" ) return TYPE_DOUBLE;

      // TODO: struct

      return TYPE_UID_MAX;
    }


    void declareBasicVariable( Iterator& it ) {
      TYPE_UID typeUID = resolveTypeName( it.Content );
      // TODO: array
      // TODO: reference

      int pointer = 0;
      std::wstring name;
      while( it.Next( tokenizer ) ) {
        if( it == TOKEN_GROUP_SPACE || it == TOKEN_GROUP_NEW_LINE )
          continue;

        if( it == TOKEN_GROUP_OPERATOR && it == L"*" )
          pointer++;

        if( it == TOKEN_GROUP_DEFINITION ) {
          name = it.Content;
          break;
        }

        Exception().Append( "expected variable name" ).Throw();
      }

      if( findVariable( name ) )
        Exception().Append( "variable already declared: " ).Append( name ).Throw();

      void* memory = ::malloc( TYPE_SIZE[typeUID] );
      ZeroMemory( memory, TYPE_SIZE[typeUID] );

      auto psVariable = std::make_shared<PreparedStatement_VARIABLE>( typeUID );
      psVariable->Source->Pointer = pointer;
      psVariable->Name = name;
      psVariable->SetReference( memory );
      builder.Append( psVariable );

      variables[name] = psVariable;
    }


    void parseDefinition( Iterator& it ) {
      if( auto statementVariable = findVariable(it.Content);
          statementVariable ) {
        builder.Append( std::move(statementVariable) );
        return;
      }

      Exception().Append( "unknown identifier: " ).Append( it.Content ).Throw();
    }
#pragma endregion

  };

  std::unordered_map<std::wstring, std::shared_ptr<PreparedStatement_VARIABLE>> Parser::variables; // DELETE ME
}