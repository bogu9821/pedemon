#pragma once

#include "Constants.h"
#include "PreparedStatement.h"
#include <string>
#include <vector>

namespace Demon {
  class PreparedStatement_USERTYPE_FIELD {
  public:
    std::wstring Name;
    std::shared_ptr<SourceContext> Source;
  
    ulong_t Sizeof() const {
      return Source->Sizeof();
    }
  };


  class PreparedStatement_USERTYPE : public PreparedStatement {
  public:
    std::vector<std::shared_ptr<PreparedStatement_USERTYPE>> Prototypes;
    std::vector<std::shared_ptr<PreparedStatement_USERTYPE_FIELD>> Fields;

    PreparedStatement_USERTYPE() : PreparedStatement() { }

    virtual ulong_t Sizeof() const {
      ulong_t total = 0;
      for( const std::shared_ptr<PreparedStatement_USERTYPE>& prototype : Prototypes )
        total += prototype->Sizeof();

      for( const std::shared_ptr<PreparedStatement_USERTYPE_FIELD>& field : Fields )
        total += field->Sizeof();

      return total;
    }
  };
}