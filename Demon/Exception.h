#pragma once

#include <string>
#include <sstream>
#include <exception>

namespace Demon {
  class Exception {
    std::ostringstream ossMessage;
  public:
    inline Exception& Append( const wchar_t* value ) {
      size_t length = wcslen( value );
      std::string buffer;
      buffer.resize( length );
      for( size_t i = 0; i < length; i++ )
        buffer[i] = (char)value[i];

      ossMessage << buffer.c_str();
      return *this;
    }

    inline Exception& Append( const std::wstring& value ) {
      return Append( value.c_str() );
    }

    template<typename T>
    inline Exception& Append( const T& value ) {
      ossMessage << value;
      return *this;
    }

    template<typename T>
    inline Exception& AppendHex( const T& value ) {
      ossMessage << std::hex << value << std::dec;
      return *this;
    }

    inline void Throw() {
       throw std::exception( ossMessage.str().c_str() );
    }
  };

#define ASSERT(x) if( !(x) ) Exception()
}