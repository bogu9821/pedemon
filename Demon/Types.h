#pragma once

namespace Demon {
  typedef char int8_t;
  typedef unsigned char uint8_t;
  typedef short int16_t;
  typedef unsigned short uint16_t;
  typedef int int32_t;
  typedef unsigned int uint32_t;
  typedef long long int64_t;
  typedef unsigned long long uint64_t;
  typedef void* mem_t;

#if _WIN64
  typedef long long long_t;
  typedef unsigned long long ulong_t;
#else
  typedef int long_t;
  typedef unsigned int ulong_t;
#endif

  struct Register {
    union {
      uint64_t u64;
      uint32_t u32;
      ulong_t  ul;
    };
  };


  union Digit {
    int8_t   Int8;
    uint8_t  UInt8;
    int16_t  Int16;
    uint16_t UInt16;
    int32_t  Int32;
    uint32_t UInt32;
    int64_t  Int64;
    uint64_t UInt64;
    long_t   Long;
    ulong_t  ULong;
    float    Float;
    double   Double;
  };
}