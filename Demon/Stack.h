#pragma once

#include <vector>
#include "Constants.h"

namespace Demon {
  class Stack {
    uint8_t* bytes;
  public:
    Stack( ulong_t size = 262'144 ) {
      bytes = new uint8_t[size];
    }

    template<typename T>
    T& GetValue( ulong_t position ) {
      return *reinterpret_cast<T*>(bytes + position);
    }

    ulong_t GetPosition() {
      return (ulong_t)bytes;
    }

    ~Stack() {
      delete[] bytes;
    }
  };
}