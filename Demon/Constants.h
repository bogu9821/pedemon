#pragma once

#include <string>
#include "Types.h"

namespace Demon {
  enum INSTRUCTION_UID {
    INSTRUCTION_PUSH,     // push
    INSTRUCTION_POP,      // pop
    INSTRUCTION_MOV,      // mov
    INSTRUCTION_LEA,      // lea
    INSTRUCTION_MUL,      // *
    INSTRUCTION_DIV,      // /
    INSTRUCTION_MOD,      // %
    INSTRUCTION_ADD,      // +
    INSTRUCTION_SUB,      // -
    INSTRUCTION_LSHIFT,   // <<
    INSTRUCTION_RSHIFT,   // >>
    INSTRUCTION_BIT_AND,  // &
    INSTRUCTION_BIT_XOR,  // ^
    INSTRUCTION_BIT_OR,   // |
    INSTRUCTION_COMPL,    // ~
    INSTRUCTION_NOT,      // !
    INSTRUCTION_NEG,      // -
    INSTRUCTION_CAST,     // (TYPE)
    INSTRUCTION_INC,      // ++
    INSTRUCTION_DEC,      // --
    INSTRUCTION_CMP,      // A-B
    INSTRUCTION_TEST,     // A!=0
    INSTRUCTION_JMP,      // jmp
    INSTRUCTION_JMP_IF,   // jmp
    INSTRUCTION_CALL,     // call
    INSTRUCTION_CALL_IF,  // call
    INSTRUCTION_CALL_API, // call api
    INSTRUCTION_RET,      // ret
    INSTRUCTION_UID_MAX
  };


  enum OPERATOR_UID {
    OPERATOR_RET,              // return
    OPERATOR_CALL,             // A()
    OPERATOR_INDEX,            // A[B]
    OPERATOR_INC_PRE,          // ++A
    OPERATOR_DEC_PRE,          // --A
    OPERATOR_CAST_REINTERPRET, // <TYPE>A
    OPERATOR_CAST_STATIC,      // (TYPE)A
    OPERATOR_COMPL,            // ~A
    OPERATOR_NOT,              // !A
    OPERATOR_NEG,              // -A
    OPERATOR_POS,              // +A
    OPERATOR_EXT,              // *A
    OPERATOR_INC_POST,         // A++
    OPERATOR_DEC_POST,         // A--
    OPERATOR_MUL,              // A*B
    OPERATOR_DIV,              // A/B
    OPERATOR_MOD,              // A%B
    OPERATOR_ADD,              // A+B
    OPERATOR_SUB,              // A-B
    OPERATOR_LSHIFT,           // A<<B
    OPERATOR_RSHIFT,           // A>>B
    OPERATOR_LESS,             // A<B
    OPERATOR_GREATER,          // A>B
    OPERATOR_LESS_OR_EQUAL,    // A<=B
    OPERATOR_GREATER_OR_EQUAL, // A>=B
    OPERATOR_EQUAL,            // A==B
    OPERATOR_NOT_EQUAL,        // A!=B
    OPERATOR_BIT_AND,          // A&B
    OPERATOR_BIT_XOR,          // A^B
    OPERATOR_BIT_OR,           // A|B
    OPERATOR_AND,              // A&&B
    OPERATOR_OR,               // A||B
    OPERATOR_ASSIGN,           // A=B
    OPERATOR_MUL_ASSIGN,       // A*=B
    OPERATOR_DIV_ASSIGN,       // A/=B
    OPERATOR_MOD_ASSIGN,       // A%=B
    OPERATOR_ADD_ASSIGN,       // A+=B
    OPERATOR_SUB_ASSIGN,       // A-=B
    OPERATOR_LSHIFT_ASSIGN,    // A<<=B
    OPERATOR_RSHIFT_ASSIGN,    // A>>=B
    OPERATOR_BIT_AND_ASSIGN,   // A&=B
    OPERATOR_BIT_XOR_ASSIGN,   // A^=B
    OPERATOR_BIT_OR_ASSIGN,    // A|=B
    OPERATOR_UID_MAX
  };


  enum OPERAND_UID {
    OPERAND_REG,         // REG
    OPERAND_IMM,         // IMM
    OPERAND_REG_MEM,     // [REG]
    OPERAND_IMM_MEM,     // [IMM]
    OPERAND_REG_REG_MEM, // [REG+REG]
    OPERAND_REG_IMM_MEM, // [REG+IMM]
    OPERAND_UID_MAX
  };


  enum REGISTER_UID {
    REGISTER_IP      = -4, // Instruction position
    REGISTER_SP      = -3, // Stack position
    REGISTER_SL      = -2, // Stack length
    REGISTER_CF      = -1, // Comparsion flag
    REGISTER_UID_MAX =  4
  };


  enum TYPE_UID {
    TYPE_VOID_PTR,
    TYPE_INT8,
    TYPE_UINT8,
    TYPE_INT16,
    TYPE_UINT16,
    TYPE_INT32,
    TYPE_UINT32,
    TYPE_INT64,
    TYPE_UINT64,
    TYPE_LONG,
    TYPE_ULONG,
    TYPE_FLOAT,
    TYPE_DOUBLE,
    TYPE_STRUCT,
    TYPE_UID_MAX,
  };


  enum SOURCE_FLAG {
    SOURCE_FLAG_OPERAND_LEFT       = 1, // Has left operand
    SOURCE_FLAG_OPERAND_RIGHT      = 2, // Has right operand
    SOURCE_FLAG_RETURN_SOURCE      = 4, // Return to the primaty operand
    SOURCE_FLAG_RETURN_DESTINATION = 8  // Return to the own memory
  };


  const ulong_t TYPE_SIZE[TYPE_UID_MAX] = {
    sizeof( void* ),
    sizeof( int8_t ),
    sizeof( uint8_t ),
    sizeof( int16_t ),
    sizeof( uint16_t ),
    sizeof( int32_t ),
    sizeof( uint32_t ),
    sizeof( int64_t ),
    sizeof( uint64_t ),
    sizeof( long_t ),
    sizeof( ulong_t ),
    sizeof( float ),
    sizeof( double ),
    0,
  };


  const wchar_t* INSTRUCTION_NAME[INSTRUCTION_UID_MAX] = {
    L"PUSH",
    L"POP",
    L"MOV",
    L"LEA",
    L"MUL",
    L"DIV",
    L"MOD",
    L"ADD",
    L"SUB",
    L"LSHIFT",
    L"RSHIFT",
    L"BIT_AND",
    L"BIT_XOR",
    L"BIT_OR",
    L"COMPL",
    L"NOT",
    L"NEG",
    L"CAST",
    L"INC",
    L"DEC",
    L"CMP",
    L"TEST",
    L"JMP",
    L"JMP_IF",
    L"CALL",
    L"CALL_IF",
    L"CALL_API",
    L"RET"
  };


  const wchar_t* TYPE_NAME[TYPE_UID_MAX] = {
    L"void*",
    L"int8",
    L"uint8",
    L"int16",
    L"uint16",
    L"int32",
    L"uint32",
    L"int64",
    L"uint64",
    L"long",
    L"ulong",
    L"float",
    L"double",
    L"struct"
  };


  inline std::wstring GetRegisterName( int8_t registerUID ) {
    switch( registerUID )
    {
      case Demon::REGISTER_IP: return L"IP";
      case Demon::REGISTER_SP: return L"SP";
      case Demon::REGISTER_SL: return L"SL";
      case Demon::REGISTER_CF: return L"CF";
    }
    return std::to_wstring( (int)registerUID );
  }



  // enum class SourceType : uint8_t {
  //   IMM,
  //   IMM_MEM,
  //   REG,
  //   REG_MEM,
  //   MAX
  // };
  // 
  // enum class OperandType : uint8_t {
  //   INT8,
  //   UINT8,
  //   INT16,
  //   UINT16,
  //   INT32,
  //   UINT32,
  //   INT64,
  //   UINT64,
  //   LONG,
  //   ULONG,
  //   FLOAT,
  //   DOUBLE,
  //   MAX
  // };
  // 
  // enum class Instruction : uint8_t {
  //   PUSH_FRAME,
  //   POP_FRAME,
  //   MOV,
  //   LEA,
  //   ADD,
  //   SUB,
  //   MUL,
  //   DIV,
  //   MOD,
  //   CAST,
  //   CMP,
  //   TEST,
  //   JMP,
  //   JMP_ABS,
  //   CALL,
  //   CALL_ABS,
  //   RET,
  //   MAX
  // };
  // 
  // enum class JumpFlag : uint8_t {
  //   UNC,  // Unconditional
  //   JE,   // cmp:  a == b
  //   JNE,  // cmp:  a != b
  //   JG,   // cmp:  a >  b
  //   JGE,  // cmp:  a >= b
  //   JL,   // cmp:  a <  b
  //   JLE,  // cmp:  a <= b
  //   JZ,   // test: a == 0
  //   JNZ   // test: a != 0
  // };
  // 
  // static ulong_t GetOperandTypeSize( OperandType type ) {
  //   static ulong_t table[] = {
  //     sizeof( int8_t ),
  //     sizeof( uint8_t ),
  //     sizeof( int16_t ),
  //     sizeof( uint16_t ),
  //     sizeof( int32_t ),
  //     sizeof( uint32_t ),
  //     sizeof( int64_t ),
  //     sizeof( uint64_t ),
  //     sizeof( long_t ),
  //     sizeof( ulong_t ),
  //     sizeof( float ),
  //     sizeof( double )
  //   };
  // 
  //   return table[(int)type];
  // }
}