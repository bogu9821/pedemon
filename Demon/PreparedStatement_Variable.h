#pragma once

#include "Code.h"
#include "PreparedStatement_Type.h"
#include <string>

namespace Demon {
  class PreparedStatement_VARIABLE : public PreparedStatement {
  public:
    std::wstring Name;

    const PreparedStatement_USERTYPE* Prototype = nullptr;
    std::shared_ptr<SourceContext> Source;

    PreparedStatement_VARIABLE( TYPE_UID typeUID )
      : PreparedStatement(), Source( std::make_shared<SourceContext>() ) {
      Source->SetBasicType( typeUID );
      Source->OperandUID = OPERAND_REG_IMM_MEM;
      Source->RegisterID[0] = REGISTER_SP;
    }

    PreparedStatement_VARIABLE( const PreparedStatement_USERTYPE* usertype )
      : PreparedStatement(), Prototype( usertype ), Source( std::make_shared<SourceContext>() ) {
      Source->SetUserType( usertype->Sizeof() );
      Source->OperandUID = OPERAND_REG_IMM_MEM;
      Source->RegisterID[0] = REGISTER_SP;
    }

    PreparedStatement_VARIABLE( std::shared_ptr<SourceContext> source )
      : PreparedStatement(), Prototype( nullptr ), Source( source ) { }

    void SetReference( void* object ) {
      Source->RegisterID[0] = -1;
      Source->RegisterID[1] = -1;
      Source->OperandUID = OPERAND_IMM_MEM;
      Source->Position = (ulong_t)object;
    }

    virtual ulong_t Sizeof() {
      return Source->Sizeof();
    }

    virtual std::shared_ptr<SourceContext> GetSourceContext() {
      return Source;
    }
  };


  class PreparedStatement_CONSTANT : public PreparedStatement {
  public:
    std::shared_ptr<SourceContext> Source;
    Digit Constant;

    PreparedStatement_CONSTANT( TYPE_UID typeUID )
      : PreparedStatement(), Source( std::make_shared<SourceContext>() ) {
      Source->OperandUID = OPERAND_IMM;
      Source->SetBasicType( typeUID );
    }

    void Compile( Code& code ) {
      switch( Source->TypeUID ) {
        case TYPE_VOID_PTR: code.Append( Constant.ULong );   break;
        case TYPE_INT8:     code.Append( Constant.Int8 );    break;
        case TYPE_UINT8:    code.Append( Constant.UInt8 );   break;
        case TYPE_INT16:    code.Append( Constant.Int16 );   break;
        case TYPE_UINT16:   code.Append( Constant.UInt16 );  break;
        case TYPE_INT32:    code.Append( Constant.Int32 );   break;
        case TYPE_UINT32:   code.Append( Constant.UInt32 );  break;
        case TYPE_INT64:    code.Append( Constant.Int64);    break;
        case TYPE_UINT64:   code.Append( Constant.UInt64 );  break;
        case TYPE_LONG:     code.Append( Constant.Long );    break;
        case TYPE_ULONG:    code.Append( Constant.ULong );   break;
        case TYPE_FLOAT:    code.Append( Constant.Float );   break;
        case TYPE_DOUBLE:   code.Append( Constant.Double );  break;
      }
    }

    virtual std::shared_ptr<SourceContext> GetSourceContext() {
      return Source;
    }
  };
}