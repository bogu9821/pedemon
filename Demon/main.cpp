#include <Windows.h>
#include <iostream>

#include "PreparedStatement_Block.h"
#include "Builder.h"
#include "Decompiler.h"
#include "Demon.h"
#include "Tokenizer.h"
#include "Parser.h"

// #include "Object.h"

namespace Demon {
  template<typename T>
  void PrintStack( Stack& stack, ulong_t position ) {
    std::wcout << stack.GetValue<T>( position ) << std::endl;
  }

  template<>
  void PrintStack<int8_t>( Stack& stack, ulong_t position ) {
    std::wcout << (int)stack.GetValue<int8_t>( position ) << std::endl;
  }

  template<typename T>
  void PrintMemory( ulong_t position ) {
    std::wcout << *reinterpret_cast<T*>( position ) << std::endl;
  }

  template<>
  void PrintMemory<int8_t>( ulong_t position ) {
    std::wcout << (int)*reinterpret_cast<int8_t*>(position) << std::endl;
  }

  void Test3() {

    Terminal::begin().endline( 2 ).setForeground( LRED ).out( "expression: " ).setForeground( LYELLOW );
    std::wstring operation;
    std::getline( std::wcin, operation );
    Terminal::begin().resetColors();

    Parser parser;
    parser.Parse( operation );
    Code& code = parser.Compile();
    auto expr = parser.GetResultExpression();

    if( code.Length() > 16 ) {
      Terminal::begin().endline();
      Decompiler decompiler( code );
      while( decompiler.Next( nullptr ) );
    }

    Machine machine;
    machine.SetCode( code );
    machine.Execute();
    Stack& stack = machine.GetStack();
    Frame& frame = machine.GetFrame();

    ulong_t position = expr->Source->Position;

    Terminal::begin().setForeground( LCYAN ).out("result: ").resetColors();

    if( expr->Source->OperandUID == OPERAND_REG_IMM_MEM ) {
      switch( expr->Source->TypeUID )
      {
        case TYPE_VOID_PTR: PrintStack<void*>   ( stack, position ); break;
        case TYPE_INT8:     PrintStack<int8_t>  ( stack, position ); break;
        case TYPE_UINT8:    PrintStack<uint8_t> ( stack, position ); break;
        case TYPE_INT16:    PrintStack<int16_t> ( stack, position ); break;
        case TYPE_UINT16:   PrintStack<uint16_t>( stack, position ); break;
        case TYPE_INT32:    PrintStack<int32_t> ( stack, position ); break;
        case TYPE_UINT32:   PrintStack<uint32_t>( stack, position ); break;
        case TYPE_INT64:    PrintStack<int64_t> ( stack, position ); break;
        case TYPE_UINT64:   PrintStack<uint64_t>( stack, position ); break;
        case TYPE_LONG:     PrintStack<long_t>  ( stack, position ); break;
        case TYPE_ULONG:    PrintStack<ulong_t> ( stack, position ); break;
        case TYPE_FLOAT:    PrintStack<float>   ( stack, position ); break;
        case TYPE_DOUBLE:   PrintStack<double>  ( stack, position ); break;
      }
    }
    else {
      switch( expr->Source->TypeUID )
      {
        case TYPE_VOID_PTR: PrintMemory<void*>   ( position ); break;
        case TYPE_INT8:     PrintMemory<int8_t>  ( position ); break;
        case TYPE_UINT8:    PrintMemory<uint8_t> ( position ); break;
        case TYPE_INT16:    PrintMemory<int16_t> ( position ); break;
        case TYPE_UINT16:   PrintMemory<uint16_t>( position ); break;
        case TYPE_INT32:    PrintMemory<int32_t> ( position ); break;
        case TYPE_UINT32:   PrintMemory<uint32_t>( position ); break;
        case TYPE_INT64:    PrintMemory<int64_t> ( position ); break;
        case TYPE_UINT64:   PrintMemory<uint64_t>( position ); break;
        case TYPE_LONG:     PrintMemory<long_t>  ( position ); break;
        case TYPE_ULONG:    PrintMemory<ulong_t> ( position ); break;
        case TYPE_FLOAT:    PrintMemory<float>   ( position ); break;
        case TYPE_DOUBLE:   PrintMemory<double>  ( position ); break;
      }
    }

    Terminal::begin().endline( 2 ).setForeground( LGRAY ).out( "- - - - - -" );
  }
}

int main() {

  // ++(888.4 * 3.3) + (int32)(5i8 * 2) * 4
  // TODO: long digits should be associated with long type !!!

  try {
    // Demon::Test2();
  }
  catch( const std::exception& e ) {
    std::cout << e.what() << std::endl;
  }

  while( true ) {
    try {
        Demon::Test3();
    }
    catch( const std::exception& e ) {
      std::cout << e.what() << std::endl;
    }
  }

  return 0;
}