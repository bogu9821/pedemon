#pragma once

#include <cstdarg>
#include <string>
#include <vector>
#include <algorithm>

namespace Demon {
  class TokenGroup {
  public:
    int UID;
    TokenGroup( int uid ) : UID( uid ) { }
    virtual size_t Match( const wchar_t* text, std::wstring& content ) = 0;

    static bool IsPartOf( const wchar_t* text, std::wstring& part ) {
      size_t textLength = wcslen( text );
      if( textLength < part.length() )
        return false;

      return memcmp( text, part.c_str(), part.length() * sizeof( wchar_t ) ) == 0;
    }
  };


  class TokenGroupSequence : public TokenGroup {
    std::wstring Symbols;
    std::wstring Range;

    bool starts( const wchar_t*& symbols ) {
      for( unsigned long i = 0; i < Symbols.size(); i++ ) {
        if( *symbols == Symbols[i] ) {
          symbols++;
          return true;
        }
      }

      return false;
    }

    bool continues( const wchar_t*& symbols ) {
      if( starts( symbols ) )
        return true;

      if( Range.length() > 0 ) {
        for( unsigned long i = 0; i < Range.size(); i++ ) {
          if( *symbols == Range[i] ) {
            symbols++;
            return true;
          }
        }
      }

      return false;
    }

  public:
    TokenGroupSequence( int uid, const wchar_t* symbols, const wchar_t* range = nullptr ) : TokenGroup( uid ) {
      Symbols = symbols;
      if( range )
        Range = range;
    }

    virtual size_t Match( const wchar_t* text, std::wstring& content ) {
      const wchar_t* iterator = text;
      if( !starts( iterator ) )
        return 0;

      size_t length = 1;
      while( continues( iterator ) )
        length++;

      if( length > 0 )
        content = std::wstring( text, length );
      
      return length; 
    }
  };


  class TokenGroupPrefix : public TokenGroupSequence {
    std::wstring Prefix;
  public:
    TokenGroupPrefix( int uid, const wchar_t* prefix, const wchar_t* symbols, const wchar_t* range = nullptr ) : TokenGroupSequence( uid, symbols, range ) {
      Prefix = prefix;
    }

    virtual size_t Match( const wchar_t* text, std::wstring& content ) {
      size_t textLength = wcslen( text );
      size_t prefixLength = Prefix.length();

      if( prefixLength > textLength )
        return 0;

      if( memcmp( text, Prefix.c_str(), prefixLength * sizeof( wchar_t ) ) != 0 )
        return 0;

      size_t result = TokenGroupSequence::Match( text + prefixLength, content );
      if( result > 0 )
        return result + prefixLength;

      return 0;
    }
  };


  class TokenGroupEnum : public TokenGroup {
    std::vector<std::wstring> Items;

    void Resort() {
      std::ranges::sort( Items,
        []( const std::wstring& left, const std::wstring& right ) {
          return left.length() > right.length();
        } );
    }
  public:
    TokenGroupEnum( int uid, std::vector<std::wstring> words ) : TokenGroup( uid ), Items( std::move( words ) ) {
      Resort();
    }

    virtual size_t Match( const wchar_t* text, std::wstring& content ) {
      for( std::wstring& word : Items ) {
        if( IsPartOf( text, word ) ) {
          content = word;
          return word.length();
        }
      }

      return 0;
    }
  };


  class TokenGroupDelimiter : public TokenGroup {
    std::wstring Open;
    std::wstring Close;
  public:
    TokenGroupDelimiter( int uid, const wchar_t* open, const wchar_t* close ) : TokenGroup( uid ) {
      Open = open;
      Close = close;
    }

    virtual size_t Match( const wchar_t* text, std::wstring& content ) {
      if( IsPartOf( text, Open ) ) {
        const wchar_t* contentStart = text + Open.length();
        const wchar_t* iterator = contentStart;

        if( Close.length() == 0 ) {
          while( *iterator != 0 && *iterator != L'\n' )
            iterator++;
          
          size_t contentLength = iterator - contentStart;
          size_t matchLength = iterator - text;

          content = std::wstring( text, contentLength );
          return matchLength;
        }
        else if( Open == Close ) {
          while( *iterator != 0 ) {
            if( *iterator == L'\\' ) {
              iterator += 2;
              continue;
            }

            if( IsPartOf( iterator, Close ) ) {
              size_t contentLength = iterator - contentStart;
              size_t matchLength = iterator - text + Close.size();

              content = std::wstring( contentStart, contentLength );
              return matchLength;
            }

            iterator++;
          }
        }
        else {
          int entries = 1;
          while( *iterator != 0 ) {
            if( *iterator == L'\\' ) {
              iterator += 2;
              continue;
            }

            if( IsPartOf( iterator, Open ) ) {
              iterator += Open.length();
              entries++;
              continue;
            }

            if( IsPartOf( iterator, Close ) ) {
              if( --entries <= 0 ) {
                size_t contentLength = iterator - contentStart;
                size_t matchLength = iterator - text + Close.size();

                content = std::wstring( contentStart, contentLength );
                return matchLength;
              }
              iterator += Close.length();
            }

            iterator++;
          }
        }
      }

      return 0;
    }
  };


  class Tokenizer {
    std::vector<TokenGroup*> Groups;
  public:
    Tokenizer( std::vector<TokenGroup*> groups ) : Groups( std::move( groups ) ) { }

    std::wstring Next( const wchar_t*& iterator, int& groupID ) {
      groupID = -1;

      std::wstring content;
      for( TokenGroup* group : Groups ) {
        size_t length = group->Match( iterator, content );
        if( length > 0 ) {
          iterator += length;
          groupID = group->UID;
          return content;
        }
      }

      content = iterator[0];
      iterator++;
      return content;
    }
  };
}