#pragma once

#include <vector>
#include "Exception.h"
#include "Constants.h"
#include "Code.h"
#include "Frame.h"
#include "Stack.h"
#include "Processor.h"

#pragma warning(push)
#pragma warning(disable:6201)
#pragma warning(disable:6385)
#pragma warning(disable:6386)

namespace Demon {
  class Machine {
    Code code;
    Frame frame;
    Stack stack;

  public:
    void SetCode( const Code& refCode ) {
      code = refCode;
      frame.Registers[REGISTER_SP].ul = stack.GetPosition();
      frame.Registers[REGISTER_SL].u64 = 0ull;
    }

  private:
    inline void* ReadSource( uint8_t type ) {
      uint8_t operand = code.GetValue<uint8_t>( frame.Registers[REGISTER_IP] );
      switch( operand ) {
        case OPERAND_REG:
        {
          int8_t regID = code.GetValue<int8_t>( frame.Registers[REGISTER_IP] );
          return &frame.Registers[regID];
        }
        case OPERAND_IMM:
        {
          void* position = code.GetPosition( frame.Registers[REGISTER_IP] );
          frame.Registers[REGISTER_IP].ul += TYPE_SIZE[type];
          return position;
        }
        case OPERAND_REG_MEM:
        {
          int8_t regID = code.GetValue<int8_t>( frame.Registers[REGISTER_IP] );
          return (void*)frame.Registers[regID].ul;
        }
        case OPERAND_IMM_MEM:
        {
          ulong_t value = code.GetValue<ulong_t>( frame.Registers[REGISTER_IP] );
          return (void*)value;
        }
        case OPERAND_REG_REG_MEM:
        {
          int8_t regID1 = code.GetValue<int8_t>( frame.Registers[REGISTER_IP] );
          int8_t regID2 = code.GetValue<int8_t>( frame.Registers[REGISTER_IP] );
          return (void*)(frame.Registers[regID1].ul + frame.Registers[regID2].ul);
        }
        case OPERAND_REG_IMM_MEM:
        {
          int8_t regID = code.GetValue<int8_t>( frame.Registers[REGISTER_IP] );
          ulong_t value = code.GetValue<ulong_t>( frame.Registers[REGISTER_IP] );
          return (void*)(frame.Registers[regID].ul + value);
        }
      }
    }

    inline void Execute_MATH_1( uint8_t instruction ) {
      uint8_t typeUID = code.GetValue<uint8_t>( frame.Registers[REGISTER_IP] );
      void* input1 = ReadSource( typeUID );
      void* output = ReadSource( typeUID );
      Processor::ResolveOperation( (TYPE_UID)typeUID, (INSTRUCTION_UID)instruction, input1, nullptr, output );
    }

    inline void Execute_MATH_2( uint8_t instruction ) {
      uint8_t typeUID = code.GetValue<uint8_t>( frame.Registers[REGISTER_IP] );
      void* input1 = ReadSource( typeUID );
      void* input2 = ReadSource( typeUID );
      void* output = ReadSource( typeUID );
      Processor::ResolveOperation( (TYPE_UID)typeUID, (INSTRUCTION_UID)instruction, input1, input2, output );
    }

    inline void Execute_CAST() {
      uint8_t typeInUID  = code.GetValue<uint8_t>( frame.Registers[REGISTER_IP] );
      uint8_t typeOutUID = code.GetValue<uint8_t>( frame.Registers[REGISTER_IP] );
      void* input1 = ReadSource( typeInUID );
      void* output = ReadSource( typeOutUID );
      Processor::ResolveConversion( (TYPE_UID)typeInUID, (TYPE_UID)typeOutUID, input1, output );
    }

  public:
    void Execute( ulong_t position = 0 ) {
      ulong_t ip = frame.Registers[REGISTER_IP].ul;
      frame.Registers[REGISTER_IP].ul = position;
      {
        bool stop = false;
        while( !stop ) {
          uint8_t instruction = code.GetValue<uint8_t>( frame.Registers[REGISTER_IP] );
          switch( instruction )
          {
            case INSTRUCTION_RET:
              stop = true;  
              break;

            case INSTRUCTION_NEG:
            case INSTRUCTION_NOT:
            case INSTRUCTION_INC:
            case INSTRUCTION_DEC:
            case INSTRUCTION_COMPL:
              Execute_MATH_1( instruction );
              break;

            case INSTRUCTION_MUL:
            case INSTRUCTION_DIV:
            case INSTRUCTION_MOD:
            case INSTRUCTION_ADD:
            case INSTRUCTION_SUB:
            case INSTRUCTION_LSHIFT:
            case INSTRUCTION_RSHIFT:
            case INSTRUCTION_BIT_AND:
            case INSTRUCTION_BIT_XOR:
            case INSTRUCTION_BIT_OR:
            case INSTRUCTION_MOV:
              Execute_MATH_2( instruction );
              break;

            case INSTRUCTION_CAST:
              Execute_CAST();
              break;
          }
        }
      }
      frame.Registers[REGISTER_IP].ul = ip;
    }

    Stack& GetStack() {
      return stack;
    }

    Frame& GetFrame() {
      return frame;
    }
  };
}

#pragma warning(pop)