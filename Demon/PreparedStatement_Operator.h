#pragma once

#include "Exception.h"
#include "Code.h"
#include "PreparedStatement_Variable.h"
#include "PreparedStatement_LABEL.h"

namespace Demon {
  class PreparedStatement_OPERATOR : public PreparedStatement {
  public:
    OPERATOR_UID UID;
    ulong_t Direction;
    ulong_t Priority;

    std::shared_ptr<SourceContext> Source;
    std::shared_ptr<PreparedStatement> Left;
    std::shared_ptr<PreparedStatement> Right;

    PreparedStatement_OPERATOR( OPERATOR_UID uid )
      : PreparedStatement(), Source( nullptr ), Left( nullptr ), Right( nullptr ) {
      UID = uid;
      Direction = OPERATOR_DIRECTION[UID];
      Priority = OPERATOR_PRIORITY[UID];
    }

    std::shared_ptr<SourceContext> GetPrimaryOperableSourceContext() {
      if( Left )
        return Left->GetSourceContext();
      
      if( Right )
        return Right->GetSourceContext();
      
      return nullptr;
    }

    void InitializeSourceContext() {
      if( Source )
        return;

      std::shared_ptr<SourceContext> primarySource = GetPrimaryOperableSourceContext();
      if( !primarySource )
        return;

      if( Direction & SOURCE_FLAG_RETURN_SOURCE ) {
        Source = primarySource;
        return;
      }
      
      Source = primarySource->Clone();
      Source->OperandUID = OPERAND_REG_IMM_MEM;
      Source->RegisterID[0] = REGISTER_SP;

    }

    std::shared_ptr<SourceContext> CreateSourceContext( TYPE_UID type ) {
      Source = std::make_shared<SourceContext>();
      if( type != TYPE_STRUCT ) {
        Source->SetBasicType( type );
        Source->OperandUID = OPERAND_REG_IMM_MEM;
        Source->RegisterID[0] = REGISTER_SP;
        Source->Position = -1;
      }
      else {
        // TODO: structure
        // void CreateSourceContext( PreparedStatement_USERTYPE type )
      }

      return Source;
    }

    void SetSourceContext( std::shared_ptr<SourceContext> source ) {
      Source = source;
    }

    void Compile( Code& code ) {
      // TODO: user type???

      TYPE_UID type1 = TYPE_UID_MAX;
      TYPE_UID type2 = Source ? Source->TypeUID : TYPE_UID_MAX;

      switch( UID ) {
        case OPERATOR_RET:
          code.Append( INSTRUCTION_RET );
          return;

        // case OPERATOR_CALL:
        // case OPERATOR_INDEX:

        case OPERATOR_INC_POST:
        case OPERATOR_INC_PRE:
          code.Append( INSTRUCTION_INC );
          break;

        case OPERATOR_DEC_POST:
        case OPERATOR_DEC_PRE:
          code.Append( INSTRUCTION_DEC );
          break;

        // TODO: right context should changed
        // case OPERATOR_CAST_REINTERPRET:
        //   if( Right->GetSourceContext()->IsReadOnly() ) {
        //     Exception().Append( "reinterpret cast cannot be used with imm value" ).Throw();
        //   }
        //   code.Append( INSTRUCTION_CAST );
        //   type2 = Right->GetSourceContext()->TypeUID;
        //   break;

        case OPERATOR_CAST_STATIC:
          code.Append( INSTRUCTION_CAST );
          type1 = Right->GetSourceContext()->TypeUID;
          break;


        case OPERATOR_COMPL:      code.Append( INSTRUCTION_COMPL ); break;
        case OPERATOR_NOT:        code.Append( INSTRUCTION_NOT ); break;
        case OPERATOR_NEG:        code.Append( INSTRUCTION_NEG ); break;
        // case OPERATOR_POS:        code.Append( INSTRUCTION_NEG ); break;
        // case OPERATOR_EXT:        code.Append( INSTRUCTION_NEG ); break;



        case OPERATOR_ADD:
        case OPERATOR_ADD_ASSIGN: 
          code.Append( INSTRUCTION_ADD );
          break;

        case OPERATOR_SUB:
        case OPERATOR_SUB_ASSIGN: 
          code.Append( INSTRUCTION_SUB );
          break;

        case OPERATOR_MUL:
        case OPERATOR_MUL_ASSIGN: 
          code.Append( INSTRUCTION_MUL );
          break;

        case OPERATOR_DIV:
        case OPERATOR_DIV_ASSIGN:       
          code.Append( INSTRUCTION_DIV );
          break;

        case OPERATOR_MOD:
        case OPERATOR_MOD_ASSIGN:       
          code.Append( INSTRUCTION_MOD );
          break;

        case OPERATOR_LSHIFT:
        case OPERATOR_LSHIFT_ASSIGN:
          code.Append( INSTRUCTION_LSHIFT );
          break;

        case OPERATOR_RSHIFT:
        case OPERATOR_RSHIFT_ASSIGN:
          code.Append( INSTRUCTION_RSHIFT );
          break;

        /*
            OPERATOR_LESS,               // A<B
            OPERATOR_GREATER,            // A>B
            OPERATOR_LESS_OR_EQUAL,      // A<=B
            OPERATOR_GREATER_OR_EQUAL,   // A>=B
            OPERATOR_EQUAL,              // A==B
            OPERATOR_NOT_EQUAL,          // A!=B
        */

        case OPERATOR_BIT_AND:
        case OPERATOR_BIT_AND_ASSIGN:
          code.Append( INSTRUCTION_BIT_AND );
          break;

        case OPERATOR_BIT_XOR:
        case OPERATOR_BIT_XOR_ASSIGN:
          code.Append( INSTRUCTION_BIT_XOR );
          break;

        case OPERATOR_BIT_OR:
        case OPERATOR_BIT_OR_ASSIGN:
          code.Append( INSTRUCTION_BIT_OR );
          break;

        case OPERATOR_ASSIGN:
          code.Append( INSTRUCTION_MOV );
          break;
      }

      if( type1 != TYPE_UID_MAX )
        code.Append( type1 );

      if( type2 != TYPE_UID_MAX )
        code.Append( type2 );
      
      if( Left ) {
        Left->GetSourceContext()->Compile( code );
        std::shared_ptr<PreparedStatement_CONSTANT> constant = std::dynamic_pointer_cast<PreparedStatement_CONSTANT>(Left);
        if( constant ) {
          if( Left->GetSourceContext() == Source ) {
            Exception().Append( "Left value cannot be const" ).Throw();
          }
          constant->Compile( code );
        }
      }
      if( Right ) {
        Right->GetSourceContext()->Compile( code );
        std::shared_ptr<PreparedStatement_CONSTANT> constant = std::dynamic_pointer_cast<PreparedStatement_CONSTANT>(Right);
        if( constant )
          constant->Compile( code );
      }
      if( Source )
        Source->Compile( code );
    }

    void WriteOperand( Code& code, SourceContext* source ) {
      source->Compile( code );
      // if( source->OperandUID == OPERAND_IMM ) {
      //   PreparedStatement_CONSTANT* constant = dynamic_cast<PreparedStatement_CONSTANT*>(Left);
      //   if( constant )
      //     constant->Compile( code );
      // }
    }

    virtual std::shared_ptr<SourceContext> GetSourceContext() {
      return Source;
    }
  };


  class PreparedStatement_OPERATOR_JMP : public PreparedStatement_OPERATOR {
  public:
    PreparedStatement_LABEL* Label;
  };
}