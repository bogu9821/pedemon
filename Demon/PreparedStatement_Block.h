#pragma once

#include "Code.h"
#include "PreparedStatement_Expression.h"

namespace Demon {
  class PreparedStatement_BLOCK : public PreparedStatement {
  public:
    uint64_t StackLength = 0;

    std::vector<std::shared_ptr<SourceContext>> Sources;
    std::vector<std::shared_ptr<PreparedStatement_VARIABLE>> Variables;
    std::vector<std::shared_ptr<PreparedStatement_EXPRESSION>> Expressions;

    PreparedStatement_BLOCK() : PreparedStatement() { }

    void Append( std::shared_ptr<PreparedStatement_VARIABLE> statement ) {
      Variables.push_back( std::move(statement) );
    }

    void Append( std::shared_ptr<PreparedStatement_EXPRESSION> statement ) {
      Expressions.push_back( std::move(statement) );
    }

    void Append( std::shared_ptr<SourceContext> source ) {
      if( source )
        if( IndexOf( source ) == -1 )
          Sources.push_back( std::move(source) );
    }

    size_t IndexOf( const std::shared_ptr<SourceContext>& source ) {
      for( size_t i = 0; i < Sources.size(); i++ )
        if( source == Sources[i] )
          return i;
      
      return static_cast<size_t>(-1);
    }

    void InitializeStack() {
      Sources.clear();

      for( std::shared_ptr<PreparedStatement_EXPRESSION>& expression : Expressions ) {
        for( std::shared_ptr<PreparedStatement_VARIABLE>& variable : expression->Variables )
          Append( variable->GetSourceContext() );

        for( std::shared_ptr<PreparedStatement_OPERATOR>& op : expression->Operators )
          Append( op->GetSourceContext() );
      }

      StackLength = 0;
      for( std::shared_ptr<SourceContext>& source : Sources ) {
        if( source->UseStack() ) {
          source->Position = StackLength;
          StackLength += source->Sizeof();
        }
      }
    }

    void Compile( Code& code ) {
      InitializeStack();

      // ADD: reg[SL] + StackLength -> reg[SL]
      code.Append( INSTRUCTION_ADD ).Append( TYPE_UINT64 )
        .Append( OPERAND_REG ).Append( REGISTER_SL )
        .Append( OPERAND_IMM ).Append( StackLength )
        .Append( OPERAND_REG ).Append( REGISTER_SL );

      for( std::shared_ptr<PreparedStatement_EXPRESSION>& expression : Expressions )
        expression->Compile( code );
    }
  };
}