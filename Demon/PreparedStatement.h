#pragma once

#include <string>
#include <memory>
#include "Rules.h"
#include "Code.h"

namespace Demon {
  class SourceContext {
  public:
    TYPE_UID TypeUID;
    OPERAND_UID OperandUID;
    int8_t RegisterID[2];
    ulong_t Position = -1;
    ulong_t Size;
    uint32_t Pointer = 0;
    uint32_t Array = 1;

    void SetBasicType( TYPE_UID typeUID ) {
      TypeUID = typeUID;
      Size = TYPE_SIZE[TypeUID];
    }

    void SetUserType( ulong_t size ) {
      TypeUID = TYPE_STRUCT;
      Size = size;
    }

    ulong_t Sizeof() {
      if( Pointer > 0 )
        return sizeof( void* );

      return Size * Array;
    }

    bool UseStack() {
      return RegisterID[0] == REGISTER_SP;
    }

    std::shared_ptr<SourceContext> Clone() {
      std::shared_ptr<SourceContext> clone = std::make_shared<SourceContext>();
      clone->TypeUID       = TypeUID;
      clone->Size          = Size;
      clone->Pointer       = Pointer;
      clone->Array         = Array;
      return clone;
    }

    void Compile( Code& code ) {
      code.Append( OperandUID );

      switch( OperandUID ) {
        case OPERAND_REG:
        case OPERAND_REG_MEM:
        case OPERAND_REG_IMM_MEM:
          code.Append( RegisterID[0] );
          break;

        case OPERAND_REG_REG_MEM:
          code.Append( RegisterID[0] );
          code.Append( RegisterID[1] );
          break;
      }

      if( Position != -1 )
        code.Append( Position );
    }

    bool IsSame( SourceContext* source ) {
      if( TypeUID != source->TypeUID )
        return false;

      if( TypeUID == TYPE_STRUCT ) {
        // TODO: struct
      }

      if( Array > 1 || source->Array > 1 )
        return false;

      if( Pointer != source->Pointer )
        return false;

      return true;
    }

    bool IsInterchangeable( SourceContext* source ) {
      if( TypeUID == TYPE_STRUCT || source->TypeUID == TYPE_STRUCT )
        return false;
      
      if( Array > 1 || source->Array > 1 )
        return false;

      if( Pointer || source->Pointer )
        return false;

      return true;
    }

    bool IsReadOnly() {
      return OperandUID == OPERAND_IMM;
    }

    bool HasIMM() {
      return
        OperandUID == OPERAND_IMM ||
        OperandUID == OPERAND_IMM_MEM ||
        OperandUID == OPERAND_REG_IMM_MEM;
    }

    static int& GetItemsCount() {
      static int count = 0;
      return count;
    }

    SourceContext() {
      GetItemsCount()++;
    }

    ~SourceContext() {
      GetItemsCount()--;
    }
  };


  class PreparedStatement {
  protected:
  public:
    std::wstring Text;

    static int& GetItemsCount() {
      static int count = 0;
      return count;
    }
    PreparedStatement() {
      GetItemsCount()++;
    }

    virtual std::wstring ToString() {
      return Text;
    }

    virtual std::shared_ptr<SourceContext> GetSourceContext() {
      return nullptr;
    }

    virtual ~PreparedStatement() {
      GetItemsCount()--;
    }
  };
}