#pragma once

#include <stack>
#include "Constants.h"

namespace Demon {
  class Frame {
    Register service[REGISTER_UID_MAX];
    Register common[32];
  public:
    Register* Registers = common + REGISTER_UID_MAX;
  };
}