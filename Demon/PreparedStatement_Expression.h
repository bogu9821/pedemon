#pragma once

#include "PreparedStatement_Operator.h"
#include "PreparedStatement_Variable.h"
#include <algorithm>
#include <ranges>

namespace Demon {
  class PreparedStatement_EXPRESSION : public PreparedStatement {
  public:
    std::shared_ptr<SourceContext> Source;
    std::vector<std::shared_ptr<PreparedStatement_VARIABLE>> Variables;
    std::vector<std::shared_ptr<PreparedStatement_OPERATOR>> Operators;
    std::vector<std::shared_ptr<PreparedStatement>> UnsortedStatements;


    PreparedStatement_EXPRESSION() : PreparedStatement(), Source( nullptr ) { }


    size_t IndexOf( const std::shared_ptr<PreparedStatement>& statement ) {
      for( size_t i = 0; i < UnsortedStatements.size(); i++ )
        if( statement == UnsortedStatements[i] )
          return i;

      return static_cast<size_t>(-1);
    }


    void Erase( const std::shared_ptr<PreparedStatement>& statement ) {
      while( true ) {
        size_t id = IndexOf( statement );
        if( id == -1 )
          return;

        if( id < 0 || id >= UnsortedStatements.size() )
          Exception().Append( "statement index out of range" ).Throw();

        UnsortedStatements.erase( UnsortedStatements.begin() + id );
      }
    }


    void Erase( size_t index ) {
      UnsortedStatements.erase( UnsortedStatements.begin() + index );
    }


    void Append( std::shared_ptr<PreparedStatement> statement ) {
      UnsortedStatements.push_back( statement );
      
      if( dynamic_cast<const PreparedStatement_OPERATOR*>(statement.get()) ) {
        Operators.push_back( std::static_pointer_cast<PreparedStatement_OPERATOR>( std::move(statement)) );
        return;
      }

      if( dynamic_cast<const PreparedStatement_VARIABLE*>(statement.get()) ) {
        Variables.push_back( std::static_pointer_cast<PreparedStatement_VARIABLE>( std::move(statement)) );
        return;
      }
    }


    std::shared_ptr<PreparedStatement> GetTopStatement() {
      for (const auto& statementOperator : Operators | std::views::reverse) {
          if (statementOperator->UID != OPERATOR_INC_POST &&
              statementOperator->UID != OPERATOR_DEC_POST) {
              return statementOperator;
          }
      }

      if( UnsortedStatements.size() > 0 )
        return UnsortedStatements.front();
      
      return nullptr;
    }


    void InitializeSourceContext() {
      // The Top operator is the last object in the expression
      // that can be used as the result of the expression
      std::shared_ptr<PreparedStatement> topStatement = GetTopStatement();
      if( !topStatement )
        return;

      // For example 'return' operator cannot have a source
      std::shared_ptr<SourceContext> source = topStatement->GetSourceContext();
      if( !source )
        return;
      
      // Constant value cannot be used as an expression result
      if( !source->IsReadOnly() ) {
        Source = source;
        return;
      }

      // Same mem cell on the stack
      Source = source->Clone();
      Source->OperandUID = OPERAND_REG_IMM_MEM;
      Source->RegisterID[0] = REGISTER_SP;
      Source->Position = -1;

      // The 'Assign' operator to place this expression result on the stack
      std::shared_ptr<PreparedStatement_OPERATOR> statementOperator = std::make_shared<PreparedStatement_OPERATOR>( OPERATOR_ASSIGN );
      statementOperator->Left = std::make_shared<PreparedStatement_VARIABLE>( Source );
      statementOperator->Right = topStatement;
      statementOperator->InitializeSourceContext();
      Operators.push_back( std::move(statementOperator) );
      Erase( topStatement );
    }


    bool CreateCastStatement( std::shared_ptr<PreparedStatement_OPERATOR>& statementOperator ) {
      if( !statementOperator->Left || !statementOperator->Right )
        return false;

      std::shared_ptr<SourceContext> sourceLeft  = statementOperator->Left->GetSourceContext();
      std::shared_ptr<SourceContext> sourceRight = statementOperator->Right->GetSourceContext();

      if( sourceLeft->IsSame( sourceRight.get() ) )
        return false;

      if( !sourceLeft->IsInterchangeable( sourceRight.get() ) )
        Exception().Append( L"incompatible types" ).Throw();

      std::shared_ptr<PreparedStatement_OPERATOR> statementStaticCast =
        std::make_shared<PreparedStatement_OPERATOR>( OPERATOR_CAST_STATIC );

      statementStaticCast->Left = nullptr;
      statementStaticCast->Right = statementOperator->Right;
      statementOperator->Right = statementStaticCast;

      statementStaticCast->Source = sourceLeft->Clone();
      statementStaticCast->Source->OperandUID = OPERAND_REG_IMM_MEM;
      statementStaticCast->Source->RegisterID[0] = REGISTER_SP;
      statementStaticCast->Source->Position = -1;

      auto it = std::ranges::find( Operators, statementOperator );
      Operators.insert( it, statementStaticCast );
      return true;
    }


    void Resort() {
      if( Operators.size() > 0 ) {
        std::stable_sort( Operators.begin(), Operators.end(),
          []( const std::shared_ptr<PreparedStatement_OPERATOR>& left, const std::shared_ptr<PreparedStatement_OPERATOR>& right ) {
            return left->Priority < right->Priority;
          } );

        for( size_t i = 0; i < Operators.size(); i++ ) {
          std::shared_ptr<PreparedStatement_OPERATOR>& statementOperator = Operators[i];

          if( statementOperator->UID == OPERATOR_INC_POST ||
              statementOperator->UID == OPERATOR_DEC_POST ) {
            size_t id = IndexOf( statementOperator );
            statementOperator->Left = UnsortedStatements[id - 1];
            Erase( id );
          }
        }

        // Operators linking
        for( size_t i = 0; i < Operators.size(); i++ ) {
          std::shared_ptr<PreparedStatement_OPERATOR>& statementOperator = Operators[i];
          if( statementOperator->UID == OPERATOR_INC_POST ||
              statementOperator->UID == OPERATOR_DEC_POST )
            continue;

          size_t id = IndexOf( statementOperator );

          if( statementOperator->Direction & SOURCE_FLAG_OPERAND_RIGHT ) {
            statementOperator->Right = UnsortedStatements[id + 1];
            Erase( id + 1 );
          }

          if( statementOperator->Direction & SOURCE_FLAG_OPERAND_LEFT ) {
            statementOperator->Left = UnsortedStatements[id - 1];
            Erase( id - 1 );
          }
        }

        for( size_t i = 0; i < Operators.size(); i++ ) {
          std::shared_ptr<PreparedStatement_OPERATOR> statement = Operators[i];
          if( statement->UID == OPERATOR_CAST_STATIC )
            continue;

          if( CreateCastStatement( statement ) )
            i++;

          statement->InitializeSourceContext();
        }
      }

      InitializeSourceContext();

      // Check that all statements have a pair
      if( Operators.size() > 0 ) {
        for( std::shared_ptr<PreparedStatement_OPERATOR>& statement : Operators )
          Erase( statement );

        if( UnsortedStatements.size() > 0 ) {
          //Exception().Append( "unrelated expression found" ).Throw();
        }
      }
      else if( UnsortedStatements.size() != 1 ) {
        //Exception().Append( "unrelated expression found" ).Throw();
      }
    }


    void Compile( Code& code ) {
      for( std::shared_ptr<PreparedStatement_OPERATOR>& statement : Operators )
        statement->Compile( code );
    }


    virtual std::shared_ptr<SourceContext> GetSourceContext() {
      return Source;
    }

    std::shared_ptr<PreparedStatement> Front() {
      return UnsortedStatements.size() > 0 ?
        UnsortedStatements.front() :
        nullptr;
    }

    std::shared_ptr<PreparedStatement> Back() {
      return UnsortedStatements.size() > 0 ?
        UnsortedStatements.back() :
        nullptr;
    }
  };
}