#pragma once

#include <vector>
#include "Constants.h"

namespace Demon {
  class Code {
    std::vector<uint8_t> code;
  public:
    Code() { }

    Code( Code& proto ) {
      code = proto.code;
    }

    template<class T>
    Code& Append( T value ) {
      uint8_t* bytes = reinterpret_cast<uint8_t*>(&value);
      for( int i = 0; i < sizeof( T ); i++ )
        code.push_back( bytes[i] );

      return *this;
    }

    Code& Append( INSTRUCTION_UID value ) {
      return Append( (uint8_t)value );
    }

    Code& Append( OPERAND_UID value ) {
      return Append( (uint8_t)value );
    }

    Code& Append( REGISTER_UID value ) {
      return Append( (uint8_t)value );
    }

    Code& Append( TYPE_UID value ) {
      return Append( (uint8_t)value );
    }

    template<typename T>
    T& Reserve() {
      size_t position = code.size();
      for( size_t i = 0; i < sizeof( T ); i++ )
        code.push_back( uint8_t(0xFF) );

      return *reinterpret_cast<T*>(&code[position]);
    }

    template<typename T>
    T& GetValue( ulong_t& i ) {
      T& value = *reinterpret_cast<T*>(&code[i]);
      i += sizeof( T );
      return value;
    }

    template<typename T>
    T& GetValue( Register& i ) {
      T& value = *reinterpret_cast<T*>(&code[i.ul]);
      i.ul += sizeof( T );
      return value;
    }

    void* GetPosition( ulong_t i ) {
      return &code[i];
    }

    void* GetPosition( Register i ) {
      return &code[i.ul];
    }

    uint8_t& operator [] ( ulong_t i ) {
      return code[i];
    }

    ulong_t Length() {
      return code.size();
    }
  };
}